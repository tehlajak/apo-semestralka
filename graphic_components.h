/**
 * @file graphic_components.h
 * @author Jan Tychtl + Jakub Tehlar
 * @brief Base graphic functions, drawing buttons and ui
 * @version 1.0
 * @date 2022-05-09
 * 
 * @copyright Copyright (c) 2022
 * 
 */
#ifndef GRAPHIC_COMPONENTS_H
#define GRAPHIC_COMPONENTS_H

#include "game_core.h"

extern u_int16_t temp_red;
extern u_int16_t temp_green;
extern u_int16_t temp_blue;

extern u_int8_t temp_8_red;
extern u_int8_t temp_8_green;
extern u_int8_t temp_8_blue;

/**
 * @brief Create a button -> FINISH JAVADOC
 * 
 * @param x 
 * @param y 
 * @param w 
 * @param h 
 * @param sign 
 * @param color_text 
 * @param color_bg 
 */
void create_button(int x, int y, int w, int h, char *sign, int16_t color_text, int16_t color_bg);

/**
 * @brief Create a rectangle --> finish 
 * 
 * @param x 
 * @param y 
 * @param w 
 * @param h 
 * @param color 
 */
void create_rectangle(int x, int y, int w, int h, int16_t color);


/**
 * @brief Creates 16 bit color in format 565
 * 
 * @param red 
 * @param green 
 * @param blue 
 * @return uint16_t 
 */
uint16_t convert_to_16bit_565(uint8_t red, uint8_t green, uint8_t blue);

/**
 * @brief Converts 16 bit color to 32 bit color
 * 
 * @param color 16 bit color
 * @return uint32_t 
 */
uint32_t convert_16bit_to_32bit(uint16_t color);

#endif