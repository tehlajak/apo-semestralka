/**
 * @file main_menu.h
 * @author Jan Tychtl + Jakub Tehlar
 * @brief Main Menu drawing, input detection
 * @version 1.0
 * @date 2022-05-09
 * 
 * @copyright Copyright (c) 2022
 * 
 */
#ifndef MAINMENU_H
#define MAINMENU_H

#include "game_core.h"

// ############################################################
// ### ------------------UI FUNCTIONS-----------------------###
// ############################################################

/**
 * @brief Draw and initialize main menu
 * 
 */
void init_main_menu();

/**
 * @brief Draws background grid for our game
 * 
 */
void draw_background_grid();

/**
 * @brief DRAW EDGE AROUND MENU
 * 
 */
void draw_edge(int16_t color);

/**
 * @brief Create buttons in main menu
 * 
 */
void init_main_menu_buttons();

/**
 * @brief Draw selected button in main menu
 * 
 * @param button_case which button to draw
 */
void main_menu_create_selected_button(int button_case);

/**
 * @brief Draw unselected button in main menu
 * 
 * @param button_case which button to draw
 */
void main_menu_create_unselected_button(int button_case);

// ############################################################
// ### ------------------MOVEMENT FUNCTIONS-----------------###
// ############################################################

/**
 * @brief Checks for input in main menu screen
 * 
 */
void main_menu_input_listener();


#endif
