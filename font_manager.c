/**
 * @file font_manager.c
 * @author Jan Tychtl + Jakub Tehlar
 * @brief Working with fonts
 * @version 1.0
 * @date 2022-05-09
 * 
 * @copyright Copyright (c) 2022
 * 
 */
#include "font_manager.h"

font_descriptor_t *loaded_font;

// ############################################################
// ### ---------------SCREEN FUNCTIONS----------------------###
// ############################################################

void print_character(int x, int y, char character, uint16_t color, int scale) 
{
  const font_bits_t *ptr;

  if ((character >= loaded_font->firstchar) && (character - loaded_font->firstchar < loaded_font->size)) 
  {
    if (loaded_font->offset) 
    {
      ptr = &loaded_font->bits[loaded_font->offset[character - loaded_font->firstchar]];
    } 
    else 
    {
      int bw = (loaded_font->maxwidth + 15) / 16;
      ptr = &loaded_font->bits[(character - loaded_font->firstchar) * bw * loaded_font->height];
    }
    int char_width = character_width(character);
    int i, j;
    for (i = 0; i < loaded_font->height; i++) 
    {
      font_bits_t val = *ptr;
      for (j = 0; j < char_width; j++) 
      {
        if ((val & FONT_BITS_MASK) != 0) 
        {
          draw_scaled_pixel(x + scale * j, y + scale * i, color, 4);
        }
        val <<= 1;
      }
      ptr++;
    }
  }
}

int character_width(int c)
{
    int character_width;
    if (!loaded_font->width)
    {
        character_width = loaded_font->maxwidth;
    }
    else
    {
        character_width = loaded_font->width[c - loaded_font->firstchar];
    }
    return character_width;    
}

void print_char_string(int x, int y, char *string, uint16_t color, int space, int scaling)
{
    int new_x_pos = 0;
    while (*string != '\0')
    {
        print_character(x + (new_x_pos), y, *(string), color, scaling);
        new_x_pos += (character_width(*(string)) + space) * scaling;
        string++;
    }
}


