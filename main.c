/**
 * @file main.c
 * @author Jan Tychtl + Jakub Tehlar
 * @brief Main file, starting the game loop
 * @version 1.0
 * @date 2022-05-09
 * 
 * @copyright Copyright (c) 2022
 * 
 */
#define _POSIX_C_SOURCE 200112L

#include <string.h>
#include "game_core.h"

char* picture_path;

int main(int argc, char *argv[])
{   
    // Copy input image to MZAPO desk
    char *path = strdup(argv[0]);
    char *slash = strrchr(path, '/');
    if (slash != 0)
    {
        *slash = '\0';
    }
    else
    {
        path = "./";
    }
    asprintf(&picture_path,"%s/test.ppm", path);
    printf("File: %s\n", picture_path);
    

    printf("The program has been started : \n");
    startProgramLoop();
}
