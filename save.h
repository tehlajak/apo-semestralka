/**
 * @file save.h
 * @author Jan Tychtl + Jakub Tehlar
 * @brief Functions for displaying keyboard and handling saving drawn canvas into PPM image 
 * @version 1.0
 * @date 2022-05-16
 * 
 * @copyright Copyright (c) 2022
 * 
 */

#ifndef SAVE_H
#define SAVE_H

#include "game_core.h"

extern int key_button_selected;
extern char fileNameBuffer[MAX_FILENAME_LENGTH];

/**
 * @brief Initializes save menu 
 * 
 */
void init_save_menu();

/**
 * @brief Initializes keyboard, draws the whole keyboard
 * 
 */
void init_keyboard_layout();

/**
 * @brief Listens for user selected button and its executes
 * 
 */
void save_menu_input_listener();

/**
 * @brief Handler for blue knob when selecting characters on keyboard
 * 
 */
void save_menu_blue_knob_twist_handler();


/**
 * @brief Displays current file name
 * 
 */
void draw_current_file_name();


/**
 * @brief Key press handler, inserts that char into our filename
 * 
 * @param key_button_selected Current key selected
 */
void keyboard_button_click_handler(int key_button_selected);

/**
 * @brief Creates button for the unselected option
 * 
 * @param button_case Button from menu 
 */
void draw_keyboard_unselected_button(int current_key);

/**
 * @brief Creates new color-distinguished button for the selected option
 * 
 * @param button_case Button from menu 
 */
void draw_keyboard_selected_button(int current_key);

#endif
