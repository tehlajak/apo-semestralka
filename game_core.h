/**
 * @file game_core.c
 * @author Jan Tychtl + Jakub Tehlar
 * @brief Core of program, main loop, init peripherals
 * @version 1.0
 * @date 2022-05-09
 * 
 * @copyright Copyright (c) 2022
 * 
 */
#ifndef GAME_CORE_H
#define GAME_CORE_H

#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <time.h>
#include <unistd.h>
#include <stdbool.h>
#include <dirent.h>


// Base Files 
#include "mzapo_parlcd.h"
#include "mzapo_phys.h"
#include "mzapo_regs.h"
#include "font_types.h"

// My Files
#include "main_menu.h"
#include "canvas.h"
#include "constants.h"
#include "font_manager.h"
#include "utils.h"
#include "graphic_components.h"
#include "load.h"
#include "save.h"
#include "image_utils.h"
#include "led_utils.h"

extern int current_state;

extern uint8_t* peripherals_mem;
extern uint8_t* lcd_mem;
extern uint16_t* screen; 

extern uint16_t imageMemory[CANVAS_HEIGHT*CANVAS_WIDTH];

extern char* picture_path;

/**
 * @brief Starts the main program loop
 * 
 */
void startProgramLoop();

/**
 * @brief Inicialize all needed peripherals for our program
 * 
 */
void initPeripherals();

/**
 * @brief Checks what state the program is in and calls that state listener
 * 
 */
void current_state_listener();

#endif
