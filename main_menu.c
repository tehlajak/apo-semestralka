/**
 * @file main_menu.c
 * @author Jan Tychtl + Jakub Tehlar
 * @brief Main Menu drawing, input detection
 * @version 1.0
 * @date 2022-05-09
 * 
 * @copyright Copyright (c) 2022
 * 
 */
#include "main_menu.h"

int state_selected;

void init_main_menu()
{
    printf("-------- MAIN MENU -------- \n");
    clear_screen();
    // HEADER CREATION
    create_rectangle(0,0,SCREEN_WIDTH,MAIN_MENU_HEADER_BLOCK_HEIGHT,BLACK_16);
    print_char_string(MAIN_NAME_POSITION_X,MAIN_NAME_POSITION_Y,"APO-GRAFO",WHITE_16,1,5);
    draw_background_grid();
    draw_edge(DARK_GRAY_16);
    init_main_menu_buttons();
    // LINE SEPARATOR
    state_selected = DRAW;
    display_on_screen(lcd_mem);
}

void draw_background_grid()
{
    for (int i = 0; i < 23; i++)
    {
        create_rectangle(MAIN_MENU_LINE_UNDER_HEADER_X,MAIN_MENU_LINE_UNDER_HEADER_Y+i*10,
        MAIN_MENU_LINE_UNDER_HEADER_WIDTH,MAIN_MENU_LINE_UNDER_HEADER_HEIGHT+5,GRAY_16);
        create_rectangle(MAIN_MENU_LINE_UNDER_HEADER_X,MAIN_MENU_LINE_UNDER_HEADER_Y-10+i*10,
        MAIN_MENU_LINE_UNDER_HEADER_WIDTH,MAIN_MENU_LINE_UNDER_HEADER_HEIGHT,ORANGE_16);
    }
}

void draw_edge(int16_t color)
{
    create_rectangle(0,0,SCREEN_WIDTH,MAIN_MENU_EDGE_HEIGHT,color);
    create_rectangle(0,0,MAIN_MENU_EDGE_WIDTH,SCREEN_HEIGHT,color);
    create_rectangle(SCREEN_WIDTH-MAIN_MENU_EDGE_WIDTH,0,MAIN_MENU_EDGE_WIDTH,SCREEN_HEIGHT,color);
    create_rectangle(0,SCREEN_HEIGHT - MAIN_MENU_EDGE_HEIGHT,SCREEN_WIDTH,MAIN_MENU_EDGE_WIDTH,color);
}   


void init_main_menu_buttons()
{
    main_menu_create_selected_button(DRAW);
    main_menu_create_unselected_button(SAVE);
    main_menu_create_unselected_button(LOAD);
    main_menu_create_unselected_button(QUIT);
}

void main_menu_create_selected_button(int button_case)
{
    state_selected = button_case;
    switch (button_case)
    {
    case DRAW:
        create_button(MAIN_MENU_BUTTON_X_COORD,MAIN_MENU_DRAW_Y_COORD, 
        MAIN_MENU_BUTTON_WIDTH,MAIN_MENU_BUTTON_HEIGHT,"DRAW", BLACK_16, DARK_GREEN_16);
        break;
    case SAVE:
        create_button(MAIN_MENU_BUTTON_X_COORD,MAIN_MENU_SAVE_Y_COORD, 
        MAIN_MENU_BUTTON_WIDTH,MAIN_MENU_BUTTON_HEIGHT," SAVE", BLACK_16, DARK_GREEN_16);
        break;
    case LOAD:
        create_button(MAIN_MENU_BUTTON_X_COORD,MAIN_MENU_LOAD_Y_COORD, 
        MAIN_MENU_BUTTON_WIDTH,MAIN_MENU_BUTTON_HEIGHT," LOAD", BLACK_16, DARK_GREEN_16);
        break;
    case QUIT:
        create_button(MAIN_MENU_BUTTON_X_COORD,MAIN_MENU_QUIT_Y_COORD, 
        MAIN_MENU_BUTTON_WIDTH,MAIN_MENU_BUTTON_HEIGHT," QUIT", BLACK_16, DARK_GREEN_16);
        break;
    }
}

void main_menu_create_unselected_button(int button_case)
{
    switch (button_case)
    {
    case DRAW:
        create_button(MAIN_MENU_BUTTON_X_COORD,MAIN_MENU_DRAW_Y_COORD, 
        MAIN_MENU_BUTTON_WIDTH,MAIN_MENU_BUTTON_HEIGHT,"DRAW", WHITE_16, BROWN_16);
        break;
    case SAVE:
        create_button(MAIN_MENU_BUTTON_X_COORD,MAIN_MENU_SAVE_Y_COORD, 
        MAIN_MENU_BUTTON_WIDTH,MAIN_MENU_BUTTON_HEIGHT," SAVE", WHITE_16, BROWN_16);
        break;
    case LOAD:
        create_button(MAIN_MENU_BUTTON_X_COORD,MAIN_MENU_LOAD_Y_COORD, 
        MAIN_MENU_BUTTON_WIDTH,MAIN_MENU_BUTTON_HEIGHT," LOAD", WHITE_16, BROWN_16);
        break;
    case QUIT:
        create_button(MAIN_MENU_BUTTON_X_COORD,MAIN_MENU_QUIT_Y_COORD, 
        MAIN_MENU_BUTTON_WIDTH,MAIN_MENU_BUTTON_HEIGHT," QUIT", WHITE_16, BROWN_16);
        break;
    }
}

void main_menu_input_listener()
{
    if (blue_knob_clicked)
    {
        main_menu_create_unselected_button(state_selected);
        state_selected = (state_selected + 1) % 4;
        main_menu_create_selected_button(state_selected);
        display_on_screen(lcd_mem);
    }
    else if (green_knob_clicked)
    {
        main_menu_create_unselected_button(state_selected);
        if (state_selected == 0)
        {
            state_selected = 3;
        }
        else
        {
            // Modulo operator seems to have an issue with -1
            state_selected = (state_selected - 1) % 4;
        }
        main_menu_create_selected_button(state_selected);
        display_on_screen(lcd_mem);
    }
    else if (red_knob_clicked)
    {
        switch (state_selected)
        {
        case DRAW:
            current_state = CANVAS_STATE;
            led_lights_enter_canvas();
            init_canvas();
            break;
        case SAVE:
            current_state = SAVE_STATE;
            led_lights_enter_save_menu();
            init_save_menu();
            break;
        case LOAD:
            current_state = LOAD_STATE;
            led_lights_enter_load_menu();
            init_load_menu();
            break;
        case QUIT:
            led_lights_on_turn_off();
            //kill_leds();
            clear_screen();
            display_on_screen(lcd_mem);
            exit(0);
            break;
        }
    }
}

