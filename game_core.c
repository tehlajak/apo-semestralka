/**
 * @file game_core.c
 * @author Jan Tychtl + Jakub Tehlar
 * @brief Core of program, main loop, init peripherals
 * @version 1.0
 * @date 2022-05-09
 * 
 * @copyright Copyright (c) 2022
 * 
 */
#include "game_core.h"

int current_state;

uint8_t* peripherals_mem;
uint8_t* lcd_mem;
uint16_t* screen; 
uint16_t imageMemory[CANVAS_HEIGHT*CANVAS_WIDTH];

struct ppm_image* canvas_image;

void startProgramLoop()
{
    initPeripherals();
    clean_array_in_image_memory();
    led_lights_on_turn_on();
    
    // Starts the game in main menu
    printf("INIT MAIN MENU\n");
    current_state = MAIN_MENU_STATE;
    first_canvas_init = 0;
    init_main_menu();
    //led_lights_on_enter_menu();

    while(1)
    {
        update_knobs();
        current_state_listener();
    }
}

void initPeripherals()
{
    init_peripherals_memory();
    init_knobs();
    init_screen();
    init_leds();
    init_font();
}

void current_state_listener()
{
    switch (current_state)
    {
    case MAIN_MENU_STATE:
        main_menu_input_listener();
        break;
    case CANVAS_STATE:    
        canvas_input_listener();
        break;
    case LOAD_STATE:
        load_input_listener();
        break;
    case SAVE_STATE:
        save_menu_input_listener();
        break;
    case EXIT_STATE:
        break;
    }
    sleep(0.6);
}


