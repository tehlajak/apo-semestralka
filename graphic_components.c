/**
 * @file graphic_components.c
 * @author Jan Tychtl + Jakub Tehlar
 * @brief Base graphic functions, drawing buttons and ui
 * @version 1.0
 * @date 2022-05-09
 * 
 * @copyright Copyright (c) 2022
 * 
 */
#include "graphic_components.h"

u_int16_t temp_red;
u_int16_t temp_green;
u_int16_t temp_blue;

u_int8_t temp_8_red;
u_int8_t temp_8_green;
u_int8_t temp_8_blue;

uint32_t RGB888;


void create_button(int x, int y, int w, int h, char *sign, int16_t color_text, int16_t color_bg)
{
    create_rectangle(x, y, w, h, color_bg);
    print_char_string(x + 7, y + h / 4, sign, color_text, 1, 2);
}

void create_rectangle(int x, int y, int w, int h, int16_t color)
{
    for (int i = 0; i < h; i++)
    {
        for (int j = 0; j < w; j++)
        {
            screen[(i + y) * SCREEN_WIDTH + j + x] = color;
        }
    }
}

uint16_t convert_to_16bit_565(uint8_t red, uint8_t green, uint8_t blue)
{
    temp_blue = (blue >> 3) & 0x1f;
    temp_green = ((green >> 2) & 0x3f) << 5;
    temp_red = ((red >> 3) & 0x1f) << 11;

    return (uint16_t) (temp_blue | temp_green | temp_red);
}

uint32_t convert_16bit_to_32bit(uint16_t color)
{
    temp_8_red = ((((color >> 11) & 0x1F) * 527) + 23) >> 6;
    temp_8_green = ((((color >> 5) & 0x3F) * 259) + 33) >> 6;
    temp_8_blue = (((color & 0x1F) * 527) + 23) >> 6;

    RGB888 = temp_8_red << 16 | temp_8_green << 8 | temp_8_blue;

    return RGB888;
}
