/**
 * @file led_utils.h
 * @author Jan Tychtl + Jakub Tehlar
 * @brief Functions used for interactions with led lights
 * @version 1.0
 * @date 2022-05-11
 * 
 * @copyright Copyright (c) 2022
 * 
 */

#ifndef LED_UTILS_H
#define LED_UTILS_H

#include "game_core.h"

#define COLOR_RANGE 255

// Color codes when RGB2 is control led (event handler)
#define RGB2_ENTER_MENU_COLOR 0xA163A5
#define RGB2_CANVAS_COLOR 0x83E72B
#define RGB2_LOAD_MENU_COLOR 0x26ECE6
#define RGB2_SAVE_MENU_COLOR 0xD0EF0B
#define RGB2_SUBMIT_COLOR 0xFEC914

/**
 * @brief Set the control led (led_line, rgb1, rgb2) color 
 * 
 * @param led should be rgb2 
 * @param color 
 */
void set_led_color(volatile uint32_t* led, uint32_t color);

/**
 * @brief Several color blinks when the program starts 
 * 
 */
void led_lights_on_turn_on();

/**
 * @brief Led lights color when entering the game menu
 * 
 */
void led_lights_on_enter_menu();

/**
 * @brief RGB2 color when entering canvas
 */
void led_lights_enter_canvas();

/**
 * @brief RGB2 color when entering load image menu
 */
void led_lights_enter_load_menu();

/**
 * @brief RGB2 color when entering save image menu
 */
void led_lights_enter_save_menu();

/**
 * @brief RGB2 color when submitting
 */
void led_lights_submit();

/**
 * @brief Several color blinks when the program starts 
 * 
 */
void led_lights_on_turn_off();

#endif