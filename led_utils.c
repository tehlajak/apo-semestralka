/**
 * @file led_utils.c
 * @author Jakub Tehlar (tehlajak@fel.cvut.cz)
 * @brief Implements " led_utils.h "
 * @version 1.0
 * @date 2022-05-11
 * 
 * @copyright Copyright (c) 2022
 * 
 */

#include "led_utils.h"


void set_led_color(volatile uint32_t* led, uint32_t color)
{
    if (!led)
    {
        fprintf(stderr, "Error: Invalid LED LIGHT!\n");
        exit(ERROR_LED_CODE);
    }

    *led = color;
}

void led_lights_on_turn_on()
{
    //bool rgb1_asleep = false;
    //bool rgb2_asleep = false;
    uint32_t color1 = 0x0;
    uint32_t color2 = 0x0;
    short step1 = 20;
    short step2 = 30;
    short step3 = 7;

    for (int round = 0; round < 3; round++)
    {
        // rgb1 & rgb2 color showing
        for (int i = 0; i < COLOR_RANGE - step1; i += step1)
        {
            for (int j = 30; j < COLOR_RANGE; j += step2)
            {
                color1 = set_hex_color_val(i, j, step3); 
                set_led_color(rgb1, color1);

                for (int z = 12; z < COLOR_RANGE - step3; z += step3)
                {
                    color2 = set_hex_color_val(i, j, z);
                    set_led_color(rgb2, color2);
                }
            }
        }

        // led line color showing
        for (int j = 0; j < COLOR_RANGE; j++)
        {
            color1 = set_hex_color_val(0, 0, j);
            set_led_color(led_line, color1);
        }

        color1 = set_hex_color_val(0, 0, 0);
        set_led_color(led_line, color1);

        for (int j = 0; j < COLOR_RANGE; j++)
        {
            color1 = set_hex_color_val(0, j, 0);
            set_led_color(led_line, color1);
        }

        color1 = set_hex_color_val(0, 0, 0);
        set_led_color(led_line, color1);

        for (int j = 0; j < COLOR_RANGE; j++)
        {
            color1 = set_hex_color_val(j, 0, 0);
            set_led_color(led_line, color1);
        }
    }
    
    kill_leds();
}

void led_lights_on_enter_menu()
{

    *led_line = set_hex_color_val(255, 255, 255);
    *rgb2 = set_hex_color_val(0, 0, 255);
    sleep(1);
    *led_line = set_hex_color_val(0, 0, 0);
    *rgb2 = set_hex_color_val(0, 0, 0);

    *led_line = set_hex_color_val(255, 255, 255);
    *rgb2 = set_hex_color_val(0, 255, 0);
    sleep(1);
    *led_line = set_hex_color_val(0, 0, 0);
    *rgb2 = set_hex_color_val(0, 0, 0);

    *led_line = set_hex_color_val(255, 255, 255);
    *rgb2 = set_hex_color_val(255, 0, 0);
    sleep(1);
    *led_line = set_hex_color_val(0, 0, 0);
    *rgb2 = set_hex_color_val(0, 0, 0);

    kill_leds();
}

void led_lights_on_turn_off()
{
    *led_line = set_hex_color_val(255, 255, 255);
    sleep(1);
    *led_line = set_hex_color_val(0, 0, 0);
    sleep(1);
    *led_line = set_hex_color_val(255, 255, 255);
    sleep(1);

    kill_leds();
}

void led_lights_enter_canvas()
{
    *rgb2 = RGB2_CANVAS_COLOR;
}

void led_lights_enter_load_menu()
{
    *rgb2 = RGB2_LOAD_MENU_COLOR;
}

void led_lights_enter_save_menu()
{
    *rgb2 = RGB2_SAVE_MENU_COLOR;
}

void led_lights_submit()
{
    *rgb1 = RGB2_SUBMIT_COLOR;
}
