/**
 * @file canvas.c
 * @author Jan Tychtl + Jakub Tehlar
 * @brief Working in canvas state, handling input, drawing
 * @version 1.0
 * @date 2022-05-09
 * 
 * @copyright Copyright (c) 2022
 * 
 */
#include "canvas.h"

char stringNumber[4];
int current_color_picked;
int first_canvas_init;


int current_bursh_thickness;
int current_drawing_state;

uint8_t r_color_picker_value;
uint8_t g_color_picker_value;
uint8_t b_color_picker_value;
uint16_t current_16bit_color;

uint16_t previous_x_coordinate;
uint16_t previous_y_coordinate;

uint16_t current_x_coordinate;
uint16_t current_y_coordinate;

int8_t x_coordinate_difference;
int8_t y_coordinate_difference;

void init_canvas()
{
    printf("-------- CANVAS -------- \n");
    clear_screen();
    init_canvas_values();
    draw_canvas_header();
    *led_line = set_hex_color_val(r_color_picker_value,g_color_picker_value,b_color_picker_value);
    *rgb1 = set_hex_color_val(r_color_picker_value,g_color_picker_value,b_color_picker_value);
}

void draw_canvas_header()
{
    print_char_string(X_COORD_POSITION_X,X_COORD_POSITION_Y, "X:", BLACK_16,1, 2);
    draw_x_coordinate();
    print_char_string(Y_COORD_POSITION_X,Y_COORD_POSITION_Y, "Y:", BLACK_16,1, 2);
    draw_y_coordinate();
    // print rgb header
    print_header_rgb_values();
    create_rectangle(0,CANVAS_HEIGHT,SCREEN_WIDTH,CANVAS_LINE_HEIGHT,GRAY_16);
    create_rectangle(R_COLOR_VALUE_POSITION_X-10,CANVAS_HEIGHT,COLOR_VALUE_BACKGROUND_WIDTH,CANVAS_LINE_HEIGHT,ORANGE_16);
    // print color block
    display_thickness_value();
    draw_current_state_indicator();
    display_on_screen(lcd_mem);
}

void print_header_rgb_values()
{
    convert_int_to_string(r_color_picker_value);
    create_rectangle(R_COLOR_VALUE_POSITION_X-10, CANVAS_HEIGHT+5,COLOR_VALUE_BACKGROUND_WIDTH,SCREEN_HEIGHT-5-CANVAS_HEIGHT,RED_16);
    print_char_string(R_COLOR_VALUE_POSITION_X,COLOR_VALUE_POSITION_Y, stringNumber, BLACK_16,1, 2); //R
    convert_int_to_string(g_color_picker_value);
    create_rectangle(G_COLOR_VALUE_POSITION_X-10, CANVAS_HEIGHT+5,COLOR_VALUE_BACKGROUND_WIDTH,SCREEN_HEIGHT-5-CANVAS_HEIGHT,GREEN_16);
    print_char_string(G_COLOR_VALUE_POSITION_X,COLOR_VALUE_POSITION_Y, stringNumber, BLACK_16,1, 2); //G
    convert_int_to_string(b_color_picker_value);
    create_rectangle(B_COLOR_VALUE_POSITION_X-10, CANVAS_HEIGHT+5,COLOR_VALUE_BACKGROUND_WIDTH,SCREEN_HEIGHT-5-CANVAS_HEIGHT,BLUE_16);
    print_char_string(B_COLOR_VALUE_POSITION_X,COLOR_VALUE_POSITION_Y, stringNumber, BLACK_16,1, 2); //B
}

void init_canvas_values()
{
    current_color_picked = RED_COLOR_STATE;
    if (first_canvas_init == 0)
    {
        r_color_picker_value = 0;
        g_color_picker_value = 0;
        b_color_picker_value = 0;
        current_x_coordinate = 0;
        current_y_coordinate = 0;
        previous_x_coordinate = 0;
        previous_y_coordinate = 0;
        first_canvas_init = 1;
        current_bursh_thickness = 1;
        current_drawing_state = DRAWING_STATE;
        load_from_image_memory();
    }
    else if (first_canvas_init == 1)
    {
        load_from_image_memory();
    }
    else if (first_canvas_init == 1)
    {
        load_from_image_memory();
    }
    
}

// ############################################################
// ### ------------------MOVEMENT FUNCTIONS-----------------###
// ############################################################

void canvas_input_listener()
{
    //KNOBS
    if (green_difference > 0 || green_difference < 0)
    {
        if (current_drawing_state == ERASE_STATE)
        {
            // CLEAR CANVAS to be implemented
            // When in erase state green knob difference does not matter
        }
        else
        // ANY OTHER STATE
        {
            color_green_knob_handler();
            display_thickness_value();
            *led_line = set_hex_color_val(r_color_picker_value,g_color_picker_value,b_color_picker_value);
            *rgb1 = set_hex_color_val(r_color_picker_value,g_color_picker_value,b_color_picker_value);
            //*rgb2 = led_line_show_values(r_color_picker_value,g_color_picker_value,b_color_picker_value);
        }
    }

    //  MOVEMENT
    if (red_difference > 0 || red_difference < 0)
    {
        if (current_drawing_state == MOVING_STATE)
        {        
            color_red_knob_handler();
            previous_x_coordinate = current_x_coordinate;
        }
        else if (current_drawing_state == DRAWING_STATE)
        {
            color_red_knob_handler();
            horizontal_move_event();
        }
    }
    if (blue_difference > 0 || blue_difference < 0)
    {
        if (current_drawing_state == MOVING_STATE)
        {        
            color_blue_knob_handler();
            previous_y_coordinate = current_y_coordinate;
        }
        else if (current_drawing_state == DRAWING_STATE)
        {
            color_blue_knob_handler();
            vertical_move_event();
        }
        else if (current_drawing_state == THICKNESS_STATE)
        {
            set_thickness_handler();
            display_thickness_value();
        }
    }

    //CLICKS

    // DRAWING STATES 
    if (blue_knob_clicked)
    {
        // SWITCH CURRENT DRAWING STATE
        current_drawing_state += 1;
        if (current_drawing_state == 4)
        {
            current_drawing_state = 0;
        }
        draw_current_state_indicator();
    }

    // SWITCH COLOR KNOB
    else if (green_knob_clicked)
    {   
        // IF IN ERASE STATE
        if (current_drawing_state == ERASE_STATE)
        {
            clean_array_in_image_memory();
            clear_canvas();
        }
        else
        // ANY OTHER STATE
        {
            if (current_color_picked == 2)
            {
                current_color_picked = 0;
            }
            else
            {
                current_color_picked = (current_color_picked + 1) % 3;
            }
            canvas_selected_color();
        }
    }

    // BACK TO MAIN MENU
    else if (red_knob_clicked)
    {
        current_state = MAIN_MENU_STATE;
        init_main_menu();
        kill_leds();
        led_lights_on_enter_menu();
    }
    display_on_screen(lcd_mem);
}

void horizontal_move_event()
{
    x_coordinate_difference = current_x_coordinate - previous_x_coordinate;
    if (x_coordinate_difference > 0)
    {
        for (int i = 0; i < x_coordinate_difference; i++)
        {
            if ((previous_x_coordinate+i+current_bursh_thickness) < CANVAS_WIDTH && ((current_y_coordinate + current_bursh_thickness) < CANVAS_HEIGHT+1))
            {
                draw_canvas_scaled_pixel(previous_x_coordinate+i,current_y_coordinate,current_16bit_color,current_bursh_thickness);
            }
        }
        previous_x_coordinate = current_x_coordinate;
    }
    else
    {
        for (int i = 0; i > x_coordinate_difference; i--)
        {
            if ((previous_x_coordinate+i+current_bursh_thickness) < CANVAS_WIDTH && ((current_y_coordinate + current_bursh_thickness) < CANVAS_HEIGHT+1))
            {
                draw_canvas_scaled_pixel(previous_x_coordinate+i,current_y_coordinate,current_16bit_color,current_bursh_thickness);
            }
        }
        previous_x_coordinate = current_x_coordinate;
    }
}

void vertical_move_event()
{
    y_coordinate_difference = current_y_coordinate - previous_y_coordinate;
    if (y_coordinate_difference > 0)
    {
        for (int i = 0; i < y_coordinate_difference; i++)
        {
            if ((current_x_coordinate+current_bursh_thickness) < CANVAS_WIDTH+1 && (previous_y_coordinate + i + current_bursh_thickness) < CANVAS_HEIGHT+1)
            {
                draw_canvas_scaled_pixel(current_x_coordinate,previous_y_coordinate+i,current_16bit_color,current_bursh_thickness);
            }
        }
        previous_y_coordinate = current_y_coordinate;
    }
    else
    {
        for (int i = 0; i > y_coordinate_difference; i--)
        {
            if ((current_x_coordinate+current_bursh_thickness) < CANVAS_WIDTH+1 && (previous_y_coordinate + i + current_bursh_thickness) < CANVAS_HEIGHT+1)
            {
            draw_canvas_scaled_pixel(current_x_coordinate,previous_y_coordinate+i,current_16bit_color,current_bursh_thickness);
            }
        }
        previous_y_coordinate = current_y_coordinate;
    }
}

void color_red_knob_handler()
{
    current_x_coordinate = calculate_red_knob_value(X_COORD_CALCULATE);
    draw_x_coordinate();
}

void color_green_knob_handler()
{
    switch (current_color_picked)
    {
    case RED_COLOR_STATE:
        r_color_picker_value = calculate_knob_rgb_value(r_color_picker_value);
        convert_int_to_string(r_color_picker_value);
        create_rectangle(R_COLOR_VALUE_POSITION_X-10,CANVAS_HEIGHT,COLOR_VALUE_BACKGROUND_WIDTH,CANVAS_LINE_HEIGHT,ORANGE_16);
        create_button(R_COLOR_VALUE_POSITION_X-10,CANVAS_HEIGHT+5, 
        COLOR_VALUE_BACKGROUND_WIDTH,SCREEN_HEIGHT-5-CANVAS_HEIGHT,stringNumber, BLACK_16, RED_16);
        break;
    case GREEN_COLOR_STATE:
        g_color_picker_value = calculate_knob_rgb_value(g_color_picker_value);
        convert_int_to_string(g_color_picker_value);
        create_rectangle(G_COLOR_VALUE_POSITION_X-10,CANVAS_HEIGHT,COLOR_VALUE_BACKGROUND_WIDTH,CANVAS_LINE_HEIGHT,ORANGE_16);
        create_button(G_COLOR_VALUE_POSITION_X-10,CANVAS_HEIGHT+5, 
        COLOR_VALUE_BACKGROUND_WIDTH,SCREEN_HEIGHT-5-CANVAS_HEIGHT,stringNumber, BLACK_16, GREEN_16);
        break;
    case BLUE_COLOR_STATE:
        b_color_picker_value = calculate_knob_rgb_value(b_color_picker_value);
        convert_int_to_string(b_color_picker_value);
        create_rectangle(B_COLOR_VALUE_POSITION_X-10,CANVAS_HEIGHT,COLOR_VALUE_BACKGROUND_WIDTH,CANVAS_LINE_HEIGHT,ORANGE_16);
        create_button(B_COLOR_VALUE_POSITION_X-10,CANVAS_HEIGHT+5, 
        COLOR_VALUE_BACKGROUND_WIDTH,SCREEN_HEIGHT-5-CANVAS_HEIGHT,stringNumber, BLACK_16, BLUE_16);
        break;
    }
    //current_16bit_color = convert_to_16bit_565(r_color_picker_value,g_color_picker_value,b_color_picker_value);
}

void color_blue_knob_handler()
{
    current_y_coordinate = calculate_red_knob_value(Y_COORD_CALCULATE);
    draw_y_coordinate();
}

void set_thickness_handler()
{
    current_bursh_thickness += blue_difference;
    if (current_bursh_thickness > THICKNESS_LIMIT)
    {
        current_bursh_thickness = THICKNESS_LIMIT;
    }
    else if (current_bursh_thickness <= 1)
    {
        current_bursh_thickness = 1;
    }
}

// ############################################################
// ### -----------------CALCULATE FUNCTIONS-----------------###
// ############################################################

uint16_t calculate_red_knob_value(char state)
{
    int calculated_value;
    switch (state)
    {
    case X_COORD_CALCULATE:
        calculated_value = current_x_coordinate + red_difference;
        if (calculated_value <= 0)
        {
            return 0;
        }
        else if (calculated_value >= CANVAS_WIDTH-current_bursh_thickness)
        {
            return CANVAS_WIDTH-current_bursh_thickness;
        }
        else 
        {
            return calculated_value;
        }
        break;
    case Y_COORD_CALCULATE:
        calculated_value = current_y_coordinate + blue_difference;
        if (calculated_value <= 0)
        {
            return 0;
        }
        else if (calculated_value >= CANVAS_HEIGHT-current_bursh_thickness)
        {
            return CANVAS_HEIGHT-current_bursh_thickness;
        }
        else 
        {
            return calculated_value;
        }  
        break;
    }
    return 0;
}

uint8_t calculate_knob_rgb_value(uint8_t current_value_color)
{
    int calculated_value;
    calculated_value = current_value_color + green_difference;
    if (calculated_value <= 0)
    {
        return 0;
    }
    else if (calculated_value >= 255)
    {
        return 255;
    }
    else 
    {
        return calculated_value;
    }
}

// ############################################################
// ### -------------------DISPLAY FUNCTIONS-----------------###
// ############################################################

void display_current_color_picked(uint16_t color)
{
    create_rectangle(COLOR_BLOCK_POSITION_X,CANVAS_HEIGHT+5,COLOR_BLOCK_WIDTH,SCREEN_HEIGHT-CANVAS_HEIGHT-5,color); // CURRENT COLOR
}

void draw_current_state_indicator()
{
    switch (current_drawing_state)
    {
    case DRAWING_STATE:
        display_drawing_state();
        break;
    case MOVING_STATE:
        display_moving_state();
        break;
    case THICKNESS_STATE:
        display_thickness_state();
        break;
    case ERASE_STATE:
        display_erase_state();
        break;
    }
}

void display_moving_state()
{
    create_rectangle(X_COORD_POSITION_X,CANVAS_HEIGHT,COLOR_VALUE_BACKGROUND_WIDTH+150,CANVAS_LINE_HEIGHT,ORANGE_16);
}

void display_erase_state()
{
    create_rectangle(0,CANVAS_HEIGHT,CANVAS_WIDTH,5,RED_16);
}

void display_drawing_state()
{
    create_rectangle(0,CANVAS_HEIGHT,SCREEN_WIDTH,CANVAS_LINE_HEIGHT,GRAY_16);
    canvas_selected_color();
}

void display_thickness_state()
{
    create_rectangle(X_COORD_POSITION_X,CANVAS_HEIGHT,COLOR_VALUE_BACKGROUND_WIDTH+150,CANVAS_LINE_HEIGHT,GRAY_16);
    create_rectangle(COLOR_BLOCK_POSITION_X,CANVAS_HEIGHT,COLOR_BLOCK_WIDTH,5,ORANGE_16);
}

// RGB redraws
void display_current_r_value()
{
    convert_int_to_string(red_knob_data);
    print_char_string(R_COLOR_VALUE_POSITION_X + 10 ,COLOR_VALUE_POSITION_Y, stringNumber, BLACK_16,1, 4);
}

void display_current_g_value()
{
    convert_int_to_string(green_knob_data);
    print_char_string(G_COLOR_VALUE_POSITION_X + 10 ,COLOR_VALUE_POSITION_Y, stringNumber, BLACK_16,1, 4);
}

void display_current_b_value()
{
    convert_int_to_string(blue_knob_data);
    print_char_string(B_COLOR_VALUE_POSITION_X + 10 ,COLOR_VALUE_POSITION_Y, stringNumber, BLACK_16,1, 4);
}

void display_thickness_value()
{
    current_16bit_color = convert_to_16bit_565(r_color_picker_value,g_color_picker_value,b_color_picker_value);
    display_current_color_picked(current_16bit_color);
    convert_int_to_string(current_bursh_thickness);
    print_character(COLOR_BLOCK_POSITION_X + 5,Y_COORD_POSITION_Y,stringNumber[0], WHITE_16,2);
}

void draw_x_coordinate()
{
    convert_int_to_string(current_x_coordinate);
    create_rectangle(X_COORD_POSITION_X+40,X_COORD_POSITION_Y,60,40, WHITE_16);
    print_char_string(X_COORD_POSITION_X+40,X_COORD_POSITION_Y, stringNumber, BLACK_16,1, 2);
}

void draw_y_coordinate()
{
    convert_int_to_string(current_y_coordinate);
    create_rectangle(Y_COORD_POSITION_X+40,Y_COORD_POSITION_Y,60,40, WHITE_16);
    print_char_string(Y_COORD_POSITION_X+40,Y_COORD_POSITION_Y, stringNumber, BLACK_16,1, 2);
}

void canvas_selected_color()
{
    create_rectangle(R_COLOR_VALUE_POSITION_X-20,CANVAS_HEIGHT,220,CANVAS_LINE_HEIGHT,GRAY_16);
    switch (current_color_picked)
    {
    case RED_COLOR_STATE:
        create_rectangle(R_COLOR_VALUE_POSITION_X-10,CANVAS_HEIGHT,COLOR_VALUE_BACKGROUND_WIDTH,CANVAS_LINE_HEIGHT,ORANGE_16);
        break;
    case GREEN_COLOR_STATE:
        create_rectangle(G_COLOR_VALUE_POSITION_X-10,CANVAS_HEIGHT,COLOR_VALUE_BACKGROUND_WIDTH,CANVAS_LINE_HEIGHT,ORANGE_16);
        break;
    case BLUE_COLOR_STATE:
        create_rectangle(B_COLOR_VALUE_POSITION_X-10,CANVAS_HEIGHT,COLOR_VALUE_BACKGROUND_WIDTH,CANVAS_LINE_HEIGHT,ORANGE_16);
        break;
    }
}

void clear_canvas()
{
    for (int x = 0; x < CANVAS_WIDTH; x++)
    {
        for (int y = 0; y < CANVAS_HEIGHT; y++)
        {
            screen[y*CANVAS_WIDTH+x] = WHITE_16;
        }
    }
}


// ############################################################
// ### -------------------OTHER FUNCTIONS-------------------###
// ############################################################

void convert_int_to_string(int value)
{
    empty_convertor(stringNumber);
    sprintf(stringNumber,"%d",value);
}

void empty_convertor(char *array)
{
    for (int i = 0; i < 4; i++)
    {
        array[i] = '\0';
    }
}

void load_from_image_memory()
{
    for (int x = 0; x < CANVAS_WIDTH; x++)
    {
        for (int y = 0; y < CANVAS_HEIGHT; y++)
        {
            draw_pixel(x,y,imageMemory[y*CANVAS_WIDTH+x]);
        }
    }
}

void clean_array_in_image_memory()
{
    for (int x = 0; x < CANVAS_WIDTH; x++)
    {
        for (int y = 0; y < CANVAS_HEIGHT; y++)
        {
            imageMemory[y*CANVAS_WIDTH+x] = WHITE_16;
        }
    }
}
