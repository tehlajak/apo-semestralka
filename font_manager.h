/**
 * @file font_manager.h
 * @author Jan Tychtl + Jakub Tehlar
 * @brief Working with fonts
 * @version 1.0
 * @date 2022-05-09
 * 
 * @copyright Copyright (c) 2022
 * 
 */
#ifndef FONT_MANAGER_H
#define FONT_MANAGER_H

#include "game_core.h"

extern font_descriptor_t *loaded_font;

// ############################################################
// ### -----------------FONT FUNCTIONS----------------------###
// ############################################################

/**
 * @brief Prints a specific character on screen
 * 
 * @param x X coordinates
 * @param y Y coordinates
 * @param character Character to print 
 * @param color Character color
 * @param scale Scale value
 */
void print_character(int x, int y, char character, uint16_t color, int scale);

/**
 * @brief Get charcater width
 * 
 * @param c input character
 * @return int - character width
 */
int character_width(int c);

/**
 * @brief Print array of characters/string
 * 
 * @param x X position
 * @param y Y position
 * @param string Char array/string
 * @param color Color of the string
 * @param space Letter spacing
 * @param scaling Letter Scaling
 */
void print_char_string(int x, int y, char *string, uint16_t color, int space, int scaling);

#endif