/**
 * @file save.c
 * @author Jan Tychtl (tychtjan@fel.cvut.cz)
 * @brief Implements " save.h " 
 * @version 1.0
 * @date 2022-05-16
 * 
 * @copyright Copyright (c) 2022
 * 
 */
#include "save.h"

int key_button_selected;
char fileNameBuffer[MAX_FILENAME_LENGTH];

void init_save_menu()
{
    printf("-------- SAVE MENU -------- \n");
    clear_screen();
    // HEADER CREATION
    create_rectangle(0,0,SCREEN_WIDTH,MAIN_MENU_HEADER_BLOCK_HEIGHT,BLACK_16);
    //print_char_string(MAIN_NAME_POSITION_X,MAIN_NAME_POSITION_Y,"APO-GRAFO",WHITE_16,1,5);
    draw_background_grid();
    draw_edge(DARK_GRAY_16);
    //init_main_menu_buttons();
    init_keyboard_layout();

    display_on_screen(lcd_mem);
}

void save_menu_input_listener()
{
    if (blue_difference > 0 || blue_difference < 0)
    {
        draw_keyboard_unselected_button(key_button_selected);
        save_menu_blue_knob_twist_handler();
        draw_keyboard_selected_button(key_button_selected);
    }

    if (blue_knob_clicked)
    {
        keyboard_button_click_handler(key_button_selected);
        if (key_button_selected != KEYBOARD_BUTTON_ENTER)
            draw_current_file_name();
    }
    else if (red_knob_clicked)
    {
        current_state = MAIN_MENU_STATE;
        init_main_menu();
        //led_lights_on_enter_menu();
    }
    display_on_screen(lcd_mem);

}

void save_menu_blue_knob_twist_handler()
{
    if (blue_difference == 1 || blue_difference == 2 || blue_difference == 3|| blue_difference == 4)
    {
        blue_difference = 1;
    }
    else if (blue_difference == -1 || blue_difference == -2 || blue_difference == -3 || blue_difference == -4)
    {
        blue_difference = -1;
    }
    else if (blue_difference < -4)
    {
        blue_difference = blue_difference /4;
    }
    else
    {
        blue_difference = blue_difference /4;
    }
    
    key_button_selected += blue_difference;
    while (key_button_selected < 0 || key_button_selected > KEYBOARD_NUMBER_OF_BUTTONS)
    {
        if (key_button_selected < 0)
        {
            key_button_selected = KEYBOARD_NUMBER_OF_BUTTONS + key_button_selected + 1;
        }
        else if (key_button_selected > KEYBOARD_NUMBER_OF_BUTTONS)
        {
            key_button_selected = key_button_selected - KEYBOARD_NUMBER_OF_BUTTONS - 1;
        }
    }
}

void init_keyboard_layout()
{
    key_button_selected = KEYBOARD_BUTTON_Q;
    draw_keyboard_selected_button(KEYBOARD_BUTTON_Q);
    for (int i = 1; i < 30; i++)
    {
        draw_keyboard_unselected_button(i);
    }
}

void keyboard_button_click_handler(int key_button_selected)
{
    if (strlen(fileNameBuffer) > 12 && !((key_button_selected == KEYBOARD_BUTTON_BACKSPACE) || (key_button_selected == KEYBOARD_BUTTON_ENTER)))
    {
        printf("Filename too big\n");
        return;
    }
    
    switch (key_button_selected)
    {
        case KEYBOARD_BUTTON_Q:
            strcat(fileNameBuffer,"Q");
            break;
        case KEYBOARD_BUTTON_W:
            strcat(fileNameBuffer,"W");
            break;
        case KEYBOARD_BUTTON_E:
            strcat(fileNameBuffer,"E");
            break;
        case KEYBOARD_BUTTON_R:
            strcat(fileNameBuffer,"R");
            break;
        case KEYBOARD_BUTTON_T:
            strcat(fileNameBuffer,"T");
            break;
        case KEYBOARD_BUTTON_Y:
            strcat(fileNameBuffer,"Y");
            break;
        case KEYBOARD_BUTTON_U:
            strcat(fileNameBuffer,"U");
            break;
        case KEYBOARD_BUTTON_I:
            strcat(fileNameBuffer,"I");
            break;
        case KEYBOARD_BUTTON_O:
            strcat(fileNameBuffer,"O");
            break;
        case KEYBOARD_BUTTON_P:
            strcat(fileNameBuffer,"P");
            break;

            //SECOND ROW
        case KEYBOARD_BUTTON_A:
            strcat(fileNameBuffer,"A");
            break;
        case KEYBOARD_BUTTON_S:
            strcat(fileNameBuffer,"S");
            break;
        case KEYBOARD_BUTTON_D:
            strcat(fileNameBuffer,"D");
            break;
        case KEYBOARD_BUTTON_F:
            strcat(fileNameBuffer,"F");
            break;
        case KEYBOARD_BUTTON_G:
            strcat(fileNameBuffer,"G");
            break;
        case KEYBOARD_BUTTON_H:
            strcat(fileNameBuffer,"H");
            break;
        case KEYBOARD_BUTTON_J:
            strcat(fileNameBuffer,"J");
            break;
        case KEYBOARD_BUTTON_K:
            strcat(fileNameBuffer,"K");
            break;
        case KEYBOARD_BUTTON_L:
            strcat(fileNameBuffer,"L");
            // THIRD ROW
            break;
        case KEYBOARD_BUTTON_Z:
            strcat(fileNameBuffer,"Z");
            break;
        case KEYBOARD_BUTTON_X:
            strcat(fileNameBuffer,"X");
            break;
        case KEYBOARD_BUTTON_C:
            strcat(fileNameBuffer,"C");
            break;
        case KEYBOARD_BUTTON_V:
            strcat(fileNameBuffer,"V");
            break;
        case KEYBOARD_BUTTON_B:
            strcat(fileNameBuffer,"B");
            break;
        case KEYBOARD_BUTTON_N:
            strcat(fileNameBuffer,"N");
            break;
        case KEYBOARD_BUTTON_M:
            strcat(fileNameBuffer,"M");
            break;
        case KEYBOARD_BUTTON_UNDERSCORE:
            strcat(fileNameBuffer,"_");
            break;
        case KEYBOARD_BUTTON_SPACE:
            strcat(fileNameBuffer," ");
            break;
        case KEYBOARD_BUTTON_BACKSPACE:
            fileNameBuffer[strlen(fileNameBuffer)-1] = '\0';
            break;
        case KEYBOARD_BUTTON_ENTER:
            //executeSave();
            save_image_memory(fileNameBuffer);
            clearfileName();
            // save file and go to main menu
            current_state = MAIN_MENU_STATE;
            init_main_menu();
            break;
    }
}


void draw_keyboard_selected_button(int current_key)
{
    switch (current_key)
    {
    // FIRST ROW
    case KEYBOARD_BUTTON_Q:
        create_button(FIRST_ROW_X_CORD_START,FIRST_ROW_Y_CORD_START, 
        KEY_BUTTON_WIDTH,KEY_BUTTON_HEIGHT,"Q", BLACK_16, DARK_GREEN_16);
        break;

    case KEYBOARD_BUTTON_W:
        create_button(FIRST_ROW_X_CORD_START + (1 * KEY_BUTTON_WIDTH),FIRST_ROW_Y_CORD_START, 
        KEY_BUTTON_WIDTH,KEY_BUTTON_HEIGHT,"W", BLACK_16, DARK_GREEN_16);
        break;

    case KEYBOARD_BUTTON_E:
        create_button(FIRST_ROW_X_CORD_START + (2 * KEY_BUTTON_WIDTH),FIRST_ROW_Y_CORD_START, 
        KEY_BUTTON_WIDTH,KEY_BUTTON_HEIGHT,"E", BLACK_16, DARK_GREEN_16);
        break;

    case KEYBOARD_BUTTON_R:
        create_button(FIRST_ROW_X_CORD_START + (3 * KEY_BUTTON_WIDTH),FIRST_ROW_Y_CORD_START, 
        KEY_BUTTON_WIDTH,KEY_BUTTON_HEIGHT,"R", BLACK_16, DARK_GREEN_16);
        break;

    case KEYBOARD_BUTTON_T:
        create_button(FIRST_ROW_X_CORD_START + (4 * KEY_BUTTON_WIDTH),FIRST_ROW_Y_CORD_START, 
        KEY_BUTTON_WIDTH,KEY_BUTTON_HEIGHT,"T", BLACK_16, DARK_GREEN_16);
        break;

    case KEYBOARD_BUTTON_Y:
        create_button(FIRST_ROW_X_CORD_START + (5 * KEY_BUTTON_WIDTH),FIRST_ROW_Y_CORD_START, 
        KEY_BUTTON_WIDTH,KEY_BUTTON_HEIGHT,"Y", BLACK_16, DARK_GREEN_16);
        break;

    case KEYBOARD_BUTTON_U:
        create_button(FIRST_ROW_X_CORD_START + (6 * KEY_BUTTON_WIDTH),FIRST_ROW_Y_CORD_START, 
        KEY_BUTTON_WIDTH,KEY_BUTTON_HEIGHT,"U", BLACK_16, DARK_GREEN_16);
        break;

    case KEYBOARD_BUTTON_I:
        create_button(FIRST_ROW_X_CORD_START + (7 * KEY_BUTTON_WIDTH),FIRST_ROW_Y_CORD_START, 
        KEY_BUTTON_WIDTH,KEY_BUTTON_HEIGHT," I", BLACK_16, DARK_GREEN_16);
        break;

    case KEYBOARD_BUTTON_O:
        create_button(FIRST_ROW_X_CORD_START + (8 * KEY_BUTTON_WIDTH),FIRST_ROW_Y_CORD_START, 
        KEY_BUTTON_WIDTH,KEY_BUTTON_HEIGHT,"O", BLACK_16, DARK_GREEN_16);
        break;

    case KEYBOARD_BUTTON_P:
        create_button(FIRST_ROW_X_CORD_START + (9 * KEY_BUTTON_WIDTH),FIRST_ROW_Y_CORD_START, 
        KEY_BUTTON_WIDTH,KEY_BUTTON_HEIGHT,"P", BLACK_16, DARK_GREEN_16);
        break;

    // SECOND ROW

    case KEYBOARD_BUTTON_A:
        create_button(FIRST_ROW_X_CORD_START,SECOND_ROW_Y_CORD_START, 
        KEY_BUTTON_WIDTH,KEY_BUTTON_HEIGHT,"A", BLACK_16, DARK_GREEN_16);
        break;

    case KEYBOARD_BUTTON_S:
        create_button(FIRST_ROW_X_CORD_START + (1 * KEY_BUTTON_WIDTH),SECOND_ROW_Y_CORD_START, 
        KEY_BUTTON_WIDTH,KEY_BUTTON_HEIGHT,"S", BLACK_16, DARK_GREEN_16);
        break;

    case KEYBOARD_BUTTON_D:
        create_button(FIRST_ROW_X_CORD_START + (2 * KEY_BUTTON_WIDTH),SECOND_ROW_Y_CORD_START, 
        KEY_BUTTON_WIDTH,KEY_BUTTON_HEIGHT,"D", BLACK_16, DARK_GREEN_16);
        break;

    case KEYBOARD_BUTTON_F:
        create_button(FIRST_ROW_X_CORD_START + + (3 * KEY_BUTTON_WIDTH),SECOND_ROW_Y_CORD_START, 
        KEY_BUTTON_WIDTH,KEY_BUTTON_HEIGHT,"F", BLACK_16, DARK_GREEN_16);
        break;

    case KEYBOARD_BUTTON_G:
        create_button(FIRST_ROW_X_CORD_START + (4 * KEY_BUTTON_WIDTH),SECOND_ROW_Y_CORD_START, 
        KEY_BUTTON_WIDTH,KEY_BUTTON_HEIGHT,"G", BLACK_16, DARK_GREEN_16);
        break;

    case KEYBOARD_BUTTON_H:
        create_button(FIRST_ROW_X_CORD_START + (5 * KEY_BUTTON_WIDTH),SECOND_ROW_Y_CORD_START, 
        KEY_BUTTON_WIDTH,KEY_BUTTON_HEIGHT,"H", BLACK_16, DARK_GREEN_16);
        break;

    case KEYBOARD_BUTTON_J:
        create_button(FIRST_ROW_X_CORD_START + (6 * KEY_BUTTON_WIDTH),SECOND_ROW_Y_CORD_START, 
        KEY_BUTTON_WIDTH,KEY_BUTTON_HEIGHT,"J", BLACK_16, DARK_GREEN_16);
        break;

    case KEYBOARD_BUTTON_K:
        create_button(FIRST_ROW_X_CORD_START + (7 * KEY_BUTTON_WIDTH),SECOND_ROW_Y_CORD_START, 
        KEY_BUTTON_WIDTH,KEY_BUTTON_HEIGHT,"K", BLACK_16, DARK_GREEN_16);
        break;

    case KEYBOARD_BUTTON_L:
        create_button(FIRST_ROW_X_CORD_START + (8 * KEY_BUTTON_WIDTH),SECOND_ROW_Y_CORD_START, 
        KEY_BUTTON_WIDTH,KEY_BUTTON_HEIGHT,"L", BLACK_16, DARK_GREEN_16);
        break;

    case KEYBOARD_BUTTON_UNDERSCORE:
        create_button(FIRST_ROW_X_CORD_START + (9 * KEY_BUTTON_WIDTH),SECOND_ROW_Y_CORD_START, 
        KEY_BUTTON_WIDTH,KEY_BUTTON_HEIGHT,"_", BLACK_16, DARK_GREEN_16);
        break;

    // THIRD ROW

    case KEYBOARD_BUTTON_Z:
        create_button(FIRST_ROW_X_CORD_START,THIRD_ROW_Y_CORD_START, 
        KEY_BUTTON_WIDTH,KEY_BUTTON_HEIGHT,"Z", BLACK_16, DARK_GREEN_16);
        break;

    case KEYBOARD_BUTTON_X:
        create_button(FIRST_ROW_X_CORD_START + (1 * KEY_BUTTON_WIDTH),THIRD_ROW_Y_CORD_START, 
        KEY_BUTTON_WIDTH,KEY_BUTTON_HEIGHT,"X", BLACK_16, DARK_GREEN_16);
        break;

    case KEYBOARD_BUTTON_C:
        create_button(FIRST_ROW_X_CORD_START + (2 * KEY_BUTTON_WIDTH),THIRD_ROW_Y_CORD_START, 
        KEY_BUTTON_WIDTH,KEY_BUTTON_HEIGHT,"C", BLACK_16, DARK_GREEN_16);
        break;

    case KEYBOARD_BUTTON_V:
        create_button(FIRST_ROW_X_CORD_START + (3 * KEY_BUTTON_WIDTH),THIRD_ROW_Y_CORD_START, 
        KEY_BUTTON_WIDTH,KEY_BUTTON_HEIGHT,"V", BLACK_16, DARK_GREEN_16);
        break;

    case KEYBOARD_BUTTON_B:
        create_button(FIRST_ROW_X_CORD_START + (4 * KEY_BUTTON_WIDTH),THIRD_ROW_Y_CORD_START, 
        KEY_BUTTON_WIDTH,KEY_BUTTON_HEIGHT,"B", BLACK_16, DARK_GREEN_16);
        break;

    case KEYBOARD_BUTTON_N:
        create_button(FIRST_ROW_X_CORD_START + (5 * KEY_BUTTON_WIDTH),THIRD_ROW_Y_CORD_START, 
        KEY_BUTTON_WIDTH,KEY_BUTTON_HEIGHT,"N", BLACK_16, DARK_GREEN_16);
        break;

    case KEYBOARD_BUTTON_M:
        create_button(FIRST_ROW_X_CORD_START + (6 * KEY_BUTTON_WIDTH),THIRD_ROW_Y_CORD_START, 
        KEY_BUTTON_WIDTH,KEY_BUTTON_HEIGHT,"M", BLACK_16, DARK_GREEN_16);
        break;

    case KEYBOARD_BUTTON_BACKSPACE:
        create_button(FIRST_ROW_X_CORD_START + (7 * KEY_BUTTON_WIDTH),THIRD_ROW_Y_CORD_START, 
        KEY_BUTTON_WIDTH,KEY_BUTTON_HEIGHT,"<-", BLACK_16, DARK_GREEN_16);
        break;

    case KEYBOARD_BUTTON_SPACE:
        create_button(FIRST_ROW_X_CORD_START + (8 * KEY_BUTTON_WIDTH),THIRD_ROW_Y_CORD_START, 
        KEY_BUTTON_WIDTH,KEY_BUTTON_HEIGHT,"sp", BLACK_16, DARK_GREEN_16);
        break;

    case KEYBOARD_BUTTON_ENTER:
        create_button(FIRST_ROW_X_CORD_START + (9 * KEY_BUTTON_WIDTH),THIRD_ROW_Y_CORD_START, 
        KEY_BUTTON_WIDTH,KEY_BUTTON_HEIGHT,"->", BLACK_16, DARK_GREEN_16);
        break;
    }


}

void draw_keyboard_unselected_button(int current_key)

{
    switch (current_key)
    {

    // FIRST ROW
    case KEYBOARD_BUTTON_Q:
        create_button(FIRST_ROW_X_CORD_START,FIRST_ROW_Y_CORD_START, 
        KEY_BUTTON_WIDTH,KEY_BUTTON_HEIGHT,"Q", BLACK_16, ORANGE_16);
        break;

    case KEYBOARD_BUTTON_W:
        create_button(FIRST_ROW_X_CORD_START + (1 * KEY_BUTTON_WIDTH),FIRST_ROW_Y_CORD_START, 
        KEY_BUTTON_WIDTH,KEY_BUTTON_HEIGHT,"W", BLACK_16, ORANGE_16);
        break;

    case KEYBOARD_BUTTON_E:
        create_button(FIRST_ROW_X_CORD_START + (2 * KEY_BUTTON_WIDTH),FIRST_ROW_Y_CORD_START, 
        KEY_BUTTON_WIDTH,KEY_BUTTON_HEIGHT,"E", BLACK_16, ORANGE_16);
        break;

    case KEYBOARD_BUTTON_R:
        create_button(FIRST_ROW_X_CORD_START + (3 * KEY_BUTTON_WIDTH),FIRST_ROW_Y_CORD_START, 
        KEY_BUTTON_WIDTH,KEY_BUTTON_HEIGHT,"R", BLACK_16, ORANGE_16);
        break;

    case KEYBOARD_BUTTON_T:
        create_button(FIRST_ROW_X_CORD_START + (4 * KEY_BUTTON_WIDTH),FIRST_ROW_Y_CORD_START, 
        KEY_BUTTON_WIDTH,KEY_BUTTON_HEIGHT,"T", BLACK_16, ORANGE_16);
        break;

    case KEYBOARD_BUTTON_Y:
        create_button(FIRST_ROW_X_CORD_START + (5 * KEY_BUTTON_WIDTH),FIRST_ROW_Y_CORD_START, 
        KEY_BUTTON_WIDTH,KEY_BUTTON_HEIGHT,"Y", BLACK_16, ORANGE_16);
        break;

    case KEYBOARD_BUTTON_U:
        create_button(FIRST_ROW_X_CORD_START + (6 * KEY_BUTTON_WIDTH),FIRST_ROW_Y_CORD_START, 
        KEY_BUTTON_WIDTH,KEY_BUTTON_HEIGHT,"U", BLACK_16, ORANGE_16);
        break;

    case KEYBOARD_BUTTON_I:
        create_button(FIRST_ROW_X_CORD_START + (7 * KEY_BUTTON_WIDTH),FIRST_ROW_Y_CORD_START, 
        KEY_BUTTON_WIDTH,KEY_BUTTON_HEIGHT," I", BLACK_16, ORANGE_16);
        break;

    case KEYBOARD_BUTTON_O:
        create_button(FIRST_ROW_X_CORD_START + (8 * KEY_BUTTON_WIDTH),FIRST_ROW_Y_CORD_START, 
        KEY_BUTTON_WIDTH,KEY_BUTTON_HEIGHT,"O", BLACK_16, ORANGE_16);
        break;

    case KEYBOARD_BUTTON_P:
        create_button(FIRST_ROW_X_CORD_START + (9 * KEY_BUTTON_WIDTH),FIRST_ROW_Y_CORD_START, 
        KEY_BUTTON_WIDTH,KEY_BUTTON_HEIGHT,"P", BLACK_16, ORANGE_16);
        break;

    // SECOND ROW

    case KEYBOARD_BUTTON_A:
        create_button(FIRST_ROW_X_CORD_START,SECOND_ROW_Y_CORD_START, 
        KEY_BUTTON_WIDTH,KEY_BUTTON_HEIGHT,"A", BLACK_16, ORANGE_16);
        break;

    case KEYBOARD_BUTTON_S:
        create_button(FIRST_ROW_X_CORD_START + (1 * KEY_BUTTON_WIDTH),SECOND_ROW_Y_CORD_START, 
        KEY_BUTTON_WIDTH,KEY_BUTTON_HEIGHT,"S", BLACK_16, ORANGE_16);
        break;

    case KEYBOARD_BUTTON_D:
        create_button(FIRST_ROW_X_CORD_START + (2 * KEY_BUTTON_WIDTH),SECOND_ROW_Y_CORD_START, 
        KEY_BUTTON_WIDTH,KEY_BUTTON_HEIGHT,"D", BLACK_16, ORANGE_16);
        break;

    case KEYBOARD_BUTTON_F:
        create_button(FIRST_ROW_X_CORD_START + + (3 * KEY_BUTTON_WIDTH),SECOND_ROW_Y_CORD_START, 
        KEY_BUTTON_WIDTH,KEY_BUTTON_HEIGHT,"F", BLACK_16, ORANGE_16);
        break;

    case KEYBOARD_BUTTON_G:
        create_button(FIRST_ROW_X_CORD_START + (4 * KEY_BUTTON_WIDTH),SECOND_ROW_Y_CORD_START, 
        KEY_BUTTON_WIDTH,KEY_BUTTON_HEIGHT,"G", BLACK_16, ORANGE_16);
        break;

    case KEYBOARD_BUTTON_H:
        create_button(FIRST_ROW_X_CORD_START + (5 * KEY_BUTTON_WIDTH),SECOND_ROW_Y_CORD_START, 
        KEY_BUTTON_WIDTH,KEY_BUTTON_HEIGHT,"H", BLACK_16, ORANGE_16);
        break;

    case KEYBOARD_BUTTON_J:
        create_button(FIRST_ROW_X_CORD_START + (6 * KEY_BUTTON_WIDTH),SECOND_ROW_Y_CORD_START, 
        KEY_BUTTON_WIDTH,KEY_BUTTON_HEIGHT,"J", BLACK_16, ORANGE_16);
        break;

    case KEYBOARD_BUTTON_K:
        create_button(FIRST_ROW_X_CORD_START + (7 * KEY_BUTTON_WIDTH),SECOND_ROW_Y_CORD_START, 
        KEY_BUTTON_WIDTH,KEY_BUTTON_HEIGHT,"K", BLACK_16, ORANGE_16);
        break;

    case KEYBOARD_BUTTON_L:
        create_button(FIRST_ROW_X_CORD_START + (8 * KEY_BUTTON_WIDTH),SECOND_ROW_Y_CORD_START, 
        KEY_BUTTON_WIDTH,KEY_BUTTON_HEIGHT,"L", BLACK_16, ORANGE_16);
        break;

    case KEYBOARD_BUTTON_UNDERSCORE:
        create_button(FIRST_ROW_X_CORD_START + (9 * KEY_BUTTON_WIDTH),SECOND_ROW_Y_CORD_START, 
        KEY_BUTTON_WIDTH,KEY_BUTTON_HEIGHT,"_", BLACK_16, ORANGE_16);
        break;

    // THIRD ROW

    case KEYBOARD_BUTTON_Z:
        create_button(FIRST_ROW_X_CORD_START,THIRD_ROW_Y_CORD_START, 
        KEY_BUTTON_WIDTH,KEY_BUTTON_HEIGHT,"Z", BLACK_16, ORANGE_16);
        break;

    case KEYBOARD_BUTTON_X:
        create_button(FIRST_ROW_X_CORD_START + (1 * KEY_BUTTON_WIDTH),THIRD_ROW_Y_CORD_START, 
        KEY_BUTTON_WIDTH,KEY_BUTTON_HEIGHT,"X", BLACK_16, ORANGE_16);
        break;

    case KEYBOARD_BUTTON_C:
        create_button(FIRST_ROW_X_CORD_START + (2 * KEY_BUTTON_WIDTH),THIRD_ROW_Y_CORD_START, 
        KEY_BUTTON_WIDTH,KEY_BUTTON_HEIGHT,"C", BLACK_16, ORANGE_16);
        break;

    case KEYBOARD_BUTTON_V:
        create_button(FIRST_ROW_X_CORD_START + (3 * KEY_BUTTON_WIDTH),THIRD_ROW_Y_CORD_START, 
        KEY_BUTTON_WIDTH,KEY_BUTTON_HEIGHT,"V", BLACK_16, ORANGE_16);
        break;

    case KEYBOARD_BUTTON_B:
        create_button(FIRST_ROW_X_CORD_START + (4 * KEY_BUTTON_WIDTH),THIRD_ROW_Y_CORD_START, 
        KEY_BUTTON_WIDTH,KEY_BUTTON_HEIGHT,"B", BLACK_16, ORANGE_16);
        break;

    case KEYBOARD_BUTTON_N:
        create_button(FIRST_ROW_X_CORD_START + (5 * KEY_BUTTON_WIDTH),THIRD_ROW_Y_CORD_START, 
        KEY_BUTTON_WIDTH,KEY_BUTTON_HEIGHT,"N", BLACK_16, ORANGE_16);
        break;

    case KEYBOARD_BUTTON_M:
        create_button(FIRST_ROW_X_CORD_START + (6 * KEY_BUTTON_WIDTH),THIRD_ROW_Y_CORD_START, 
        KEY_BUTTON_WIDTH,KEY_BUTTON_HEIGHT,"M", BLACK_16, ORANGE_16);
        break;

    case KEYBOARD_BUTTON_BACKSPACE:
        create_button(FIRST_ROW_X_CORD_START + (7 * KEY_BUTTON_WIDTH),THIRD_ROW_Y_CORD_START, 
        KEY_BUTTON_WIDTH,KEY_BUTTON_HEIGHT,"<-", BLACK_16, ORANGE_16);
        break;

    case KEYBOARD_BUTTON_SPACE:
        create_button(FIRST_ROW_X_CORD_START + (8 * KEY_BUTTON_WIDTH),THIRD_ROW_Y_CORD_START, 
        KEY_BUTTON_WIDTH,KEY_BUTTON_HEIGHT,"sp", BLACK_16, ORANGE_16);
        break;

    case KEYBOARD_BUTTON_ENTER:
        create_button(FIRST_ROW_X_CORD_START + (9 * KEY_BUTTON_WIDTH),THIRD_ROW_Y_CORD_START, 
        KEY_BUTTON_WIDTH,KEY_BUTTON_HEIGHT,"->", BLACK_16, ORANGE_16);
        break;
    }
}

void draw_current_file_name()
{
    create_rectangle(30,40,420,40,BLACK_16);
    print_char_string(30,40,fileNameBuffer,WHITE_16,1,2);
}

