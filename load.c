/**
 * @file load.c
 * @author Jan Tychtl (tychtjan@fel.cvut.cz)
 * @brief Implements " load.h " 
 * @version 1.0
 * @date 2022-05-16
 * 
 * @copyright Copyright (c) 2022
 * 
 */
#include "load.h"

int current_file_selected;
int load_menu_current_state;

void init_load_menu()
{
    printf("-------- LOAD MENU -------- \n");
    clear_screen();
    // HEADER CREATION
    create_rectangle(0,0,SCREEN_WIDTH,MAIN_MENU_HEADER_BLOCK_HEIGHT,BLACK_16);
    draw_background_grid();
    //draw_edge(DARK_GRAY_16);

    // Load ppm image names into memory
    search_ppm_in_dir();
    print_ppm_files();

    load_menu_current_state = LOAD_NEXT;
    init_load_menu_buttons();
    current_file_selected = 0;
    draw_file_name_in_load();

    display_on_screen(lcd_mem);
}

void load_input_listener()
{
    if (blue_knob_clicked)
    {
        load_menu_create_unselected_button(load_menu_current_state);
        load_menu_current_state = (load_menu_current_state + 1) % 4;
        load_menu_create_selected_button(load_menu_current_state);
        display_on_screen(lcd_mem);
    }
    else if (green_knob_clicked)
    {
        load_menu_create_unselected_button(load_menu_current_state);
        if (load_menu_current_state == 0)
        {
            load_menu_current_state = 3;
        }
        else
        {
            
            // Modulo operator seems to have an issue with -1
            load_menu_current_state = (load_menu_current_state - 1) % 4;
        }
        load_menu_create_selected_button(load_menu_current_state);
        display_on_screen(lcd_mem);
    }

    else if (red_knob_clicked)
    {
        switch (load_menu_current_state)
        {
        case LOAD_LOAD:
            load_image(files[current_file_selected]);
            current_state = CANVAS_STATE;
            init_canvas();
            break;
        case LOAD_NEXT:
            next_file_show();
            break;
        case LOAD_REMOVE:
            remove_file(files[current_file_selected]);
            search_ppm_in_dir();
            current_file_selected = 0;
            if (array_len == 0)
            {
                clear_first_string();
            }
            create_rectangle(30,40,420,40,BLACK_16);
            draw_file_name_in_load();
            break;
        case LOAD_RETURN:
            current_state = MAIN_MENU_STATE;
            init_main_menu();
            break;
        }
    }
    display_on_screen(lcd_mem);
}

void clear_first_string()
{
    for (int i = 0; i < strlen(files[current_file_selected]); i++)
    {
        files[current_file_selected][i] = '\0';
    }
}

void next_file_show()
{
    current_file_selected++;
        if (current_file_selected >= array_len)
        {
            current_file_selected = 0;
        }
        
        draw_file_name_in_load();
}

void draw_file_name_in_load()
{
    create_rectangle(30,40,420,40,BLACK_16);
    print_char_string(30,40,files[current_file_selected],WHITE_16,1,2);
}

void init_load_menu_buttons()
{
   load_menu_create_unselected_button(LOAD_LOAD);
   load_menu_create_selected_button(LOAD_NEXT);
   load_menu_create_unselected_button(LOAD_REMOVE);
   load_menu_create_unselected_button(LOAD_RETURN);
}

void load_menu_create_unselected_button(int button_case)
{
    switch (button_case)
    {
    case LOAD_LOAD:
        create_button(MAIN_MENU_BUTTON_X_COORD - 30,MAIN_MENU_DRAW_Y_COORD, 
        MAIN_MENU_BUTTON_WIDTH + 50,MAIN_MENU_BUTTON_HEIGHT,"    LOAD", WHITE_16, BROWN_16);
        break;
    case LOAD_NEXT:
        create_button(MAIN_MENU_BUTTON_X_COORD - 30,MAIN_MENU_SAVE_Y_COORD, 
        MAIN_MENU_BUTTON_WIDTH + 50,MAIN_MENU_BUTTON_HEIGHT,"    NEXT", WHITE_16, BROWN_16);
        break;
    case LOAD_REMOVE:
        create_button(MAIN_MENU_BUTTON_X_COORD - 30,MAIN_MENU_LOAD_Y_COORD, 
        MAIN_MENU_BUTTON_WIDTH + 50,MAIN_MENU_BUTTON_HEIGHT," REMOVE", WHITE_16, BROWN_16);
        break;
    case LOAD_RETURN:
        create_button(MAIN_MENU_BUTTON_X_COORD - 30,MAIN_MENU_QUIT_Y_COORD, 
        MAIN_MENU_BUTTON_WIDTH + 50,MAIN_MENU_BUTTON_HEIGHT," RETURN", WHITE_16, BROWN_16);
        break;
    }
}

void load_menu_create_selected_button(int button_case)
{
    switch (button_case)
    {
    case LOAD_LOAD:
        create_button(MAIN_MENU_BUTTON_X_COORD -30,MAIN_MENU_DRAW_Y_COORD, 
        MAIN_MENU_BUTTON_WIDTH + 50,MAIN_MENU_BUTTON_HEIGHT,"    LOAD", WHITE_16, DARK_GREEN_16);
        break;
    case LOAD_NEXT:
        create_button(MAIN_MENU_BUTTON_X_COORD -30,MAIN_MENU_SAVE_Y_COORD, 
        MAIN_MENU_BUTTON_WIDTH + 50,MAIN_MENU_BUTTON_HEIGHT,"    NEXT", WHITE_16, DARK_GREEN_16);
        break;
    case LOAD_REMOVE:
        create_button(MAIN_MENU_BUTTON_X_COORD -30,MAIN_MENU_LOAD_Y_COORD, 
        MAIN_MENU_BUTTON_WIDTH + 50,MAIN_MENU_BUTTON_HEIGHT," REMOVE", WHITE_16, DARK_GREEN_16);
        break;
    case LOAD_RETURN:
        create_button(MAIN_MENU_BUTTON_X_COORD -30,MAIN_MENU_QUIT_Y_COORD, 
        MAIN_MENU_BUTTON_WIDTH + 50,MAIN_MENU_BUTTON_HEIGHT," RETURN", WHITE_16, DARK_GREEN_16);
        break;
    }
}

