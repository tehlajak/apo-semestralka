/**
 * @file image_utils.c
 * @author tehlajak@fel.cvut.cz 
 * @brief 
 * @version 1.0
 * @date 2022-05-10
 * 
 * @copyright Copyright (c) 2022
 * 
 */

#include "image_utils.h"

char files[MAX_FILE_AMOUNT][500];
int array_len;

void load_image(const char* fname) {
    if (!fname) {
        fprintf(stderr, "Error: Invalid name of the image\n");
        exit(ERROR_IMG_CODE);
    }
    FILE* f = fopen(fname, "rb");
    if (!f) {
        fprintf(stderr, "Error: Could not open the file %s\n", fname);
        exit(ERROR_IMG_CODE);
    }
    int width, height, rgb_scale;
    if (fscanf(f, "P6\n %d\n %d\n %d\n", &width, &height, &rgb_scale) != 3 || (rgb_scale != COLOR_RANGE)) {
        fprintf(stderr, "Error: Invalid format of PPM image!\n");
        exit(ERROR_IMG_CODE);
    }

    unsigned char* tmp_img = malloc(3 * CANVAS_HEIGHT * CANVAS_WIDTH * sizeof(unsigned char));
    if (!tmp_img) {
        //error
        exit(ERROR_IMG_CODE);
    }

    if (fread(tmp_img, 3*CANVAS_WIDTH*CANVAS_HEIGHT,1,f) != 1) {
        fprintf(stderr, "Error: Something bad happened!\n");
        exit(ERROR_IMG_CODE);
    }

    uint8_t r,g,b; // r,g,b values
    uint16_t converted_color; // covnerted color in RGB565
    int j = 0;
    for (int i = 0; i < CANVAS_HEIGHT*CANVAS_WIDTH*3; i+=3) {
        r = tmp_img[i];
        g = tmp_img[i+1];
        b = tmp_img[i+2];
        converted_color = convert_to_16bit_565(r, g, b);
        imageMemory[j] = converted_color;
        j++;
    }
    free(tmp_img);
    tmp_img = NULL;
    fclose(f);
}

ppm_image* load_ppm_image(const char* fname)
{
    // should be (0; 255)
    int rbg_scale;
    
    if (!fname){ 
        fprintf(stderr, "Error: Invalid name of the ppm image");
        exit(ERROR_IMG_CODE);
    }

    FILE* file = fopen(fname, "rb");
    if (!file) {
        fprintf(stderr, "Error: Could not open the given image!");
        exit(ERROR_IMG_CODE);
    }
    
    // init pmm image
    ppm_image* img;
    img = malloc(sizeof(ppm_image));
    if (!img) {
        fprintf(stderr,"Error: Could not allocate enough memory for the ppm image!");
        exit(ERROR_IMG_CODE);
    }
    
    if ( (fscanf(file, "P6\n %d\n %d\n %d\n", &img->width, &img->height, &rbg_scale)) != 3 || rbg_scale != COLOR_RANGE
                        || img->width != CANVAS_WIDTH || img->height != CANVAS_HEIGHT) {
        fprintf(stderr, "Error: Invalid format of ppm image!");
        exit(ERROR_IMG_CODE);
    }

    // init image pixels
    img->data = malloc(img->width * img->height * sizeof(ppm_pixel));
    if (!img->data) {
        fprintf(stderr, "Error: Could not allocate enough memory for the pixels!");
        exit(ERROR_IMG_CODE);
    }

    if ( fread(img->data, 3 * img->width, img->height, file) != img->height ) {
        fprintf(stderr, "Error: While loading image!");
        exit(ERROR_IMG_CODE);
    }

    fclose(file);
    return img;
}

void set_rgb_on_pixel(ppm_image* img,int x, int y, __uint8_t r, __uint8_t g, __uint8_t b)
{
    if (!img || !img->data) {
        fprintf(stderr, "Error: Invalid image pointer!\n");
        exit(ERROR_IMG_CODE);
    }
    img->data[x * img->width + y].R = r;
    img->data[x * img->width + y].G = g;
    img->data[x * img->width + y].B = b;
}

ppm_image* init_ppm_image(int w, int h)
{
    ppm_image* img = malloc(sizeof(ppm_image));
    if (!img) {
        fprintf(stderr, "Error: Could not allocate enough memory for the ppm image!\n");
        exit(ERROR_IMG_CODE);
    }
    img->width = w;
    img->height = h;
    
    img->data = malloc(img->width * img->height * sizeof(ppm_pixel));
    if (!img->data) {
        fprintf(stderr, "Error: Could not allocate enough memory for the ppm pixels data!\n");
        exit(ERROR_IMG_CODE);
    }
    return img;
}

void save_ppm_image(const char* fname, ppm_image* img)
{
    if (!fname || !img) {
        fprintf(stderr, "Error: Invalid parameters");
        exit(ERROR_IMG_CODE);
    }
    
    FILE* file = fopen(fname, "wb");
    if (!file){
        fprintf(stderr, "Error: Invalid file!");
        exit(ERROR_IMG_CODE);
    }

    fprintf(file, "P6\n %d\n %d\n %d\n", img->width, img->height, COLOR_RANGE);
    fwrite(img->data, 3 * img->width, img->height, file);
    fclose(file);
}

void search_ppm_in_dir() {
    char* DIR_PATH = ".";
    struct dirent *de;

    DIR *dr = opendir(DIR_PATH);

    if (!dr) {
        fprintf(stderr, "Error: Could not open directory %s\n", DIR_PATH);
        exit(ERROR_IMG_CODE);
    }

    int i = 0;
    int j = 0;
    while ((de = readdir(dr))) {
        char *name = de->d_name;
        if (strstr(name, ".ppm")) {
            clearFilesBuffer(files[i]);
            for (j = 0; j < strlen(name); j++) {
                files[i][j] = name[j];
            }
            files[i][j+1] = '\0'; // name ending char
            i++;
        }
    }
    
        
    array_len = i;
    closedir(dr);
}

void print_ppm_files() {
    for (int i = 0; i < array_len; i++) {
        printf("%s\n", files[i]);
    }
}

void remove_file(char* fname) {
    if (!fname) {
        fprintf(stderr, "Error: Invalid pointer to name passed!\n");
        exit(ERROR_IMG_CODE);
    }
    if (remove(fname) == 0) {
        printf(" >> INFO: Deleted [ %s ]\n", fname);

    }
    else {
        printf(">> INFO: Could not delete [ %s ]\n", fname);
    }
}

void clearfileName()
{
    for (int i = 0; i < MAX_FILENAME_LENGTH; i++)
    {
        fileNameBuffer[i] = '\0';
    }
}

void clearFilesBuffer(char* name)
{
    for (int j = 0; j < 500; j++) 
    {
        name[j] = '\0';
    }
}


