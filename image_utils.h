/**
 * @file image_utils.h
 * @author Jan Tychtl + Jakub Tehlar
 * @brief Utilites used for image save, also includes old and unused ppm struct functions
 * @version 1.0
 * @date 2022-05-10
 * 
 * @copyright Copyright (c) 2022
 * 
 */

#ifndef IMAGE_UTILS_H
#define IMAGE_UTILS_H 

#define MAX_FILE_AMOUNT 20
#include "game_core.h"

typedef struct ppm_pixel
{
    unsigned char R, G, B;
} ppm_pixel;

typedef struct ppm_image
{
    ppm_pixel* data;
    int width, height; // currently supported format only
                       //   with defined width and height
} ppm_image;

extern char files[MAX_FILE_AMOUNT][500];
extern int array_len;

/**
 * Load image into an array (without struct ppm_image being used)
 * @param fname
 */
void load_image(const char* fname);

/**
 * @brief Loading image from PPM file 
 * @param fname file name
 * @return ppm_image* pointer to loaded image
 */
ppm_image* load_ppm_image(const char* fname);

/**
 * @brief Allocates memory for ppm image, returns pointer to it
 * 
 * @param w  width
 * @param h  height
 * @return ppm_image* 
 */
ppm_image* init_ppm_image(int w, int h);

/**
 * @brief Set the rgb on pixel object
 * 
 * @param img image pointer
 * @param x x coord on the canvas
 * @param y y coord on the canvas
 * @param r red pixel color
 * @param g green pixel color
 * @param b blue pixel color
 */
void set_rgb_on_pixel(ppm_image* img, int x, int y, __uint8_t r, __uint8_t g, __uint8_t b);

/**
 * @brief Saving existing image into a file
 * @param fname  name of the file to be saved to
 * @param img  pointer to the image 
 */
void save_ppm_image(const char* fname, ppm_image* img);

/**
 * Search given directory for ppm images
 * @return List of ppm files found in the directory
 */
void search_ppm_in_dir();

/**
 * Print found files (.ppm)
 */
void print_ppm_files();

/**
 * Remove file from directory
 * @param fname
 */
void remove_file(char* fname);

/**
 * @brief Clears filename buffer
 * 
 */
void clearfileName();

/**
 * @brief Test fucntion for clearing files buffer
 * 
 */
void clearFilesBuffer(char* name);


#endif
