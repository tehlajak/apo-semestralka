/**
 * @file canvas.h
 * @author Jan Tychtl + Jakub Tehlar
 * @brief Working in canvas state, handling input, drawing
 * @version 1.0
 * @date 2022-05-09
 * 
 * @copyright Copyright (c) 2022
 * 
 */
#ifndef CANVAS_H
#define CANVAS_H

#include "game_core.h"

extern int first_canvas_init;
extern int current_color_picked;
extern int current_drawing_state;

extern int current_bursh_thickness;

extern uint8_t r_color_picker_value;
extern uint8_t g_color_picker_value;
extern uint8_t b_color_picker_value;
extern uint16_t current_16bit_color;

//extern uint16_t previous_x_coordinate;
//extern uint16_t previous_y_coordinate;

//extern uint16_t current_x_coordinate;
//extern uint16_t current_y_coordinate;

//extern int8_t x_coordinate_difference;
//extern int8_t y_coordinate_difference;

// ############################################################
// ### ------------------INIT FUNCTIONS---------------------###
// ############################################################

/**
 * @brief Init canvas state
 * 
 */
void init_canvas();

/**
 * @brief Draws the canvas header
 * 
 */
void draw_canvas_header();

/**
 * @brief Sets default values in canvas
 * 
 */
void init_canvas_values();

/**
 * @brief Prints current RGB values in panel
 * 
 */
void print_header_rgb_values();

// ############################################################
// ### ------------------MOVEMENT FUNCTIONS-----------------###
// ############################################################

/**
 * @brief Main listener for all inputs in drawing section of the program
 * 
 */
void canvas_input_listener();

/**
 * @brief Execute when moving with right,left
 * 
 */
void horizontal_move_event();

/**
 * @brief Execute when moving up,down
 * 
 */
void vertical_move_event();

/**
 * @brief Handles changes in red knob
 * 
 */
void color_red_knob_handler();

/**
 * @brief Handles changes in green knob
 * 
 */
void color_green_knob_handler();

/**
 * @brief Handles changes in blue knob
 * 
 */
void color_blue_knob_handler();

/**
 * @brief Set the thickness 
 * 
 */
void set_thickness_handler();

// ############################################################
// ### -----------------CALCULATE FUNCTIONS-----------------###
// ############################################################

/**
 * @brief Calculates red knob value for state that the program is in
 * 
 * @param state Current canvas state
 * @return uint16_t red knob value in that state
 */
uint16_t calculate_red_knob_value(char state);

/**
 * @brief Used for calculating rgb values
 * 
 * @param current_value_color R,G or B color state
 * @return uint8_t R,G or B value, from range 0-255
 */
uint8_t calculate_knob_rgb_value(uint8_t current_value_color);

// ############################################################
// ### -------------------DISPLAY FUNCTIONS-----------------###
// ############################################################

/**
 * @brief Redraws indicator showing the current color
 * 
 * @param color Current color picked
 */
void display_current_color_picked(uint16_t color);

/**
 * @brief Handler for different state indicator drawing
 * 
 */
void draw_current_state_indicator();

/**
 * @brief Displays indicator when user is in moving state
 * 
 */
void display_moving_state();

/**
 * @brief Displays indicator when user is in erase state
 * 
 */
void display_erase_state();

/**
 * @brief Displays indicator when user is in drawing state
 * 
 */
void display_drawing_state();

/**
 * @brief Displays indicator when user is in setting thickness state
 * 
 */
void display_thickness_state();

// RGB Redraws
/**
 * @brief Draws R value in the RGB part of the program
 * 
 */
void display_current_r_value();

/**
 * @brief Draws G value in the RGB part of the program
 * 
 */
void display_current_g_value();

/**
 * @brief Draws B value in the RGB part of the program
 * 
 */
void display_current_b_value();
// RGB Redraws

/**
 * @brief Display current thickness value with also current color value in background
 * 
 */
void display_thickness_value();

/**
 * @brief Redraws X coordinate
 * 
 */
void draw_x_coordinate();

/**
 * @brief Redraws Y coordinate
 * 
 */
void draw_y_coordinate();

/**
 * @brief Show currently choosen RGB color value 
 * 
 */
void canvas_selected_color();

void clear_canvas();

// ############################################################
// ### -------------------OTHER FUNCTIONS-------------------###
// ############################################################

/**
 * @brief Used to transform int to string for character print
 * 
 * @param value Any integer
 */
void convert_int_to_string(int value);

/**
 * @brief Used for clearing temp string array
 * 
 * @param array 
 */
void empty_convertor(char *array);

/**
 * @brief Load image from memory again into place
 * 
 */
void load_from_image_memory();

/**
 * @brief Allocate image memory background to white color
 * 
 */
void clean_array_in_image_memory();

#endif 