/**
 * @file load.h
 * @author Jan Tychtl + Jakub Tehlar
 * @brief Functions for LOADING MENU implementation
 * @version 1.0
 * @date 2022-05-16
 * 
 * @copyright Copyright (c) 2022
 * 
 */
#ifndef LOAD_H
#define LOAD_H

#include "game_core.h"

extern int current_file_selected;

/**
 * @brief Initializes the load menu 
 * 
 */
void init_load_menu();

/**
 * @brief Listens for user selected option
 * 
 */
void load_input_listener();

/**
 * @brief Shows the next stored PPM image  
 * 
 */
void next_file_show();

/**
 * @brief Draws the name of the PPM image in header of the load menu 
 * 
 */
void draw_file_name_in_load();

/**
 * @brief Creates new color-distinguished button for the selected option
 * 
 * @param button_case Button from menu 
 */
void load_menu_create_selected_button(int button_case);

/**
 * @brief Creates button for the unselected option
 * 
 * @param button_case Button from menu 
 */
void load_menu_create_unselected_button(int button_case);

/**
 * @brief Initializes the load menu buttons
 * 
 */
void init_load_menu_buttons();

/**
 * @brief Replaces the displayed file name in header when deleted by a user
 * 
 */
void clear_first_string();

#endif
