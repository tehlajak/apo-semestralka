/**
 * @file utils.c
 * @author Jan Tychtl 
 * @author Jakub Tehlar
 * @brief Functions for initializations and working with peripherals
 * @version 1.0
 * @date 2022-05-03
 * 
 * @copyright Copyright (c) 2022
 * 
 */
#include "utils.h"

// CLICK DATA
bool prev_red_knob_clicked;
bool prev_green_knob_clicked;
bool prev_blue_knob_clicked;

bool red_knob_clicked;
bool green_knob_clicked;
bool blue_knob_clicked;

volatile uint32_t* knobs_state;

// KNOB DATA VALUES
uint8_t knob_data; 

uint8_t red_knob_data; 
uint8_t green_knob_data; 
uint8_t blue_knob_data;

uint8_t prev_red_knob_data;
uint8_t prev_green_knob_data;
uint8_t prev_blue_knob_data;

int8_t red_difference;
int8_t green_difference;
int8_t blue_difference;

volatile uint32_t* led_line;
volatile uint32_t* rgb1;
volatile uint32_t* rgb2;

// ############################################################
// ### -----------------INITIALIZATION----------------------###
// ############################################################

void init_peripherals_memory()
{
    screen = malloc(SCREEN_WIDTH * SCREEN_HEIGHT * sizeof(uint16_t));
    if (!screen) {
        fprintf(stderr, "Allocation Error: Could not allocate space for screen!\n");
        exit(-1);
    }

    lcd_mem = map_phys_address(PARLCD_REG_BASE_PHYS, PARLCD_REG_SIZE, 0);
    if (!lcd_mem) {
        fprintf(stderr, "Mapping Error: Could not allocate the memory for the lcd!\n");
        exit(-1);
    }

    peripherals_mem = map_phys_address(SPILED_REG_BASE_PHYS, SPILED_REG_SIZE, 0);
    if (!peripherals_mem) {
        fprintf(stderr, "Mapping Error: Could not map the memory for the periperals!\n");
        exit(-1);
    } 
}

void init_leds() {
    led_line = (volatile uint32_t*) (peripherals_mem + SPILED_REG_LED_LINE_o);
    rgb1 = (volatile uint32_t*) (peripherals_mem + SPILED_REG_LED_RGB1_o);
    rgb2 = (volatile uint32_t*) (peripherals_mem + SPILED_REG_LED_RGB2_o);

    *led_line = BLACK_32;
    kill_leds();
}

void init_knobs()
{
    knobs_state = (volatile uint32_t*) (peripherals_mem + SPILED_REG_KNOBS_8BIT_o);

    red_knob_clicked = 0;
    green_knob_clicked = 0;
    blue_knob_clicked = 0;

    prev_red_knob_clicked = 0;
    prev_green_knob_clicked = 0;
    prev_blue_knob_clicked = 0;

}

void init_font()
{
    loaded_font = &font_winFreeSystem14x16;
}

void init_screen()
{   
    parlcd_write_cmd(lcd_mem, 0x2c);
    
    int ptr = 0;
    uint32_t color = WHITE_32;
    for (int y = 0; y < SCREEN_HEIGHT; y++)
    {
        for (int x = 0; x < SCREEN_WIDTH; x++)
        {
            screen[ptr] = color;
            parlcd_write_data(lcd_mem, screen[ptr++]);
        }
    }
}

// ############################################################
// ### -----------------KNOB FUNCTIONS----------------------###
// ############################################################

void update_knobs()
{
    red_knob_clicked = 0;
    green_knob_clicked = 0;
    blue_knob_clicked = 0;

    knob_data = (*knobs_state) >> 24;
    red_knob_data = (*knobs_state >> 16) & 0xFF;
    green_knob_data = (*knobs_state >> 8) & 0xFF;
    blue_knob_data = (*knobs_state) & 0xFF;

    red_difference = compute_difference(red_knob_data, prev_red_knob_data);
    green_difference = compute_difference(green_knob_data, prev_green_knob_data);
    blue_difference = compute_difference(blue_knob_data, prev_blue_knob_data);

    prev_red_knob_data = red_knob_data;
    prev_green_knob_data = green_knob_data;
    prev_blue_knob_data = blue_knob_data;

    /*
    printf("Red difference: %d, Green difference: %d, Blue difference: %d\n",
     red_difference, green_difference, blue_difference);
    */
    

    // RISING EDGE CLICK
    bool current_red_click = (bool) (knob_data & RED_MASK);
    bool current_green_click = (bool) (knob_data & GREEN_MASK);
    bool current_blue_click = (bool) (knob_data & BLUE_MASK);


    // RED CLICK
    if (prev_red_knob_clicked == 0 && current_red_click)
    {
        //printf("--------------- RED CLICK -----------\n");
        red_knob_clicked = current_red_click;
        prev_red_knob_clicked = 1;
    }
    else if (prev_red_knob_clicked == 1 && !(current_red_click))
    {
        prev_red_knob_clicked = 0;
    }

    // GREEN CLICK
    if (prev_green_knob_clicked == 0 && current_green_click)
    {
        //printf("--------------- GREEN CLICK -----------\n");
        green_knob_clicked = current_green_click;
        prev_green_knob_clicked = 1;
    }
    else if (prev_green_knob_clicked == 1 && !(current_green_click))
    {
        prev_green_knob_clicked = 0;
    }

    // BLUE CLICK
    if (prev_blue_knob_clicked == 0 && current_blue_click)
    {
        //printf("----------- ---- BLUE CLICK -----------\n");
        blue_knob_clicked = current_blue_click;
        prev_blue_knob_clicked = 1;
    }
    else if (prev_blue_knob_clicked == 1 && !(current_blue_click))
    {
        prev_blue_knob_clicked = 0;
    }

    /*
    red_knob_clicked = (bool) (knob_data & RED_MASK);
    green_knob_clicked = (bool)(knob_data & GREEN_MASK);
    blue_knob_clicked = (bool) (knob_data & BLUE_MASK);
    */
}

uint8_t compute_difference(uint8_t current_data, uint8_t prev_data)
{
    int8_t diffrence = 0;
    if (current_data > prev_data)
    {
        diffrence = current_data - prev_data;
        if (diffrence > 200)
        {
            diffrence = COLOR_RANGE - current_data + 1 - prev_data;
        }
    }
    else if (current_data < prev_data)
    {
        diffrence = -(prev_data - current_data);
        if (diffrence < -200)
        {
            diffrence = -(prev_data - COLOR_RANGE + current_data + 1);
        }
    }
    //prev_data = current_data;
    return diffrence;
}

// ############################################################
// ### ------------------LED FUNCTIONS----------------------###
// ############################################################

//NEW FUNCTION
void change_rgb1(uint8_t *mem_base, uint32_t color)
{
    *(volatile uint32_t *)(mem_base + SPILED_REG_LED_RGB1_o) = color;
}

//NEW FUNCTION
void change_rgb2(uint8_t *mem_base, uint32_t colour)
{
    *(volatile uint32_t *)(mem_base + SPILED_REG_LED_RGB2_o) = colour;
}

void kill_leds() {
    *led_line = BLACK_32;
    *rgb1 = BLACK_32;
    *rgb2 = BLACK_32;
}

uint32_t set_hex_color_val(uint8_t red_val, uint8_t green_val, uint8_t blue_val) {
    uint32_t val = red_val*16*16*16*16 + green_val*16*16 + blue_val; 
    return val;
}

// ############################################################
// ### ---------------SCREEN FUNCTIONS----------------------###
// ############################################################

//NEW FUNCTION
void draw_pixel(int x, int y, uint16_t color)
{
  if ((x >= 0 && x < SCREEN_WIDTH) && (y >= 0 && y < SCREEN_HEIGHT)) 
  {
    screen[x + SCREEN_WIDTH * y] = color;
  }
}

void draw_scaled_pixel(int x, int y, uint16_t color, int scale)
{
    int i, j;
    for (i = 0; i < scale; i++)
    {
        for (j = 0; j < scale; j++)
        {
            draw_pixel(x + i, y + j, color);
        }
    }
}

void clear_screen()
{
    for (int i = 0; i < SCREEN_WIDTH * SCREEN_HEIGHT; i++)
    {
        screen[i] = WHITE_16;
    }
}

void display_on_screen(uint8_t *lcd_mem)
{
    parlcd_write_cmd(lcd_mem, 0x2c);
    for (int i = 0; i < SCREEN_WIDTH * SCREEN_HEIGHT; i++)
    {
        parlcd_write_data(lcd_mem, screen[i]);
    }
}

void draw_canvas_scaled_pixel(int x, int y, uint16_t color, int scale)
{
    int i, j;
    for (i = 0; i < scale; i++)
    {
        for (j = 0; j < scale; j++)
        {
            draw_pixel(x + i, y + j, color);
            imageMemory[(y+j) * SCREEN_WIDTH + x + i] = color;
        }
    }
}

void list_files_in_directory()
{
    DIR *d;
    struct dirent *dir;
    d = opendir(".");
    if (d)
    {
        while ((dir = readdir(d)) != NULL)
        {
            printf("%s\n", dir->d_name);
        }
        closedir(d);
    }
}

void save_image_memory(char* inputName)
{
    //time_t mytime = time(NULL);
    char fname[20];
    strcpy(fname,inputName);
    char appendPPM[5] = ".ppm";

    //sprintf(fname,"%d", (int)(mytime));
    strcat(fname,appendPPM);
    printf("\nName saved : %s\n", fname);

    /*
    if (!fname) {
        fprintf(stderr, "Error: Invalid parameters");
        exit(ERROR_IMG_CODE);
    }
    */
    
    FILE* file = fopen(fname, "wb");
    if (!file)
    {
        fprintf(stderr, "Error: Invalid file!");
        exit(ERROR_IMG_CODE);
    }

    printf("Saving file \n");
    fprintf(file, "P6\n %d\n %d\n %d\n", CANVAS_WIDTH, CANVAS_HEIGHT, COLOR_RANGE);

    uint16_t color =0;
    u_int8_t buffer[3];

    for (int i = 0; i < CANVAS_HEIGHT*CANVAS_WIDTH; i++)
    {
        color = imageMemory[i];

        buffer[0] = ((((color >> 11) & 0x1F) * 527) + 23) >> 6;
        buffer[1] = ((((color >> 5) & 0x3F) * 259) + 33) >> 6;
        buffer[2] = (((color & 0x1F) * 527) + 23) >> 6;
        //printf("Colors saved : R - %d, G - %d, B - %d\n", buffer[0],buffer[1],buffer[2]);
        fwrite(&buffer[0],3,1,file);
    }
    fclose(file);
}




