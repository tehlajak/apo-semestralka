/**
 * @file utils.h
 * @author Jan Tychtl + Jakub Tehlar
 * @brief Functions for initializations and working with peripherals
 * @version 1.0
 * @date 2022-05-03
 * 
 * @copyright Copyright (c) 2022
 * 
 */
#ifndef UTILS_H
#define UTILS_H

#include "game_core.h"

extern bool prev_red_knob_clicked;
extern bool prev_green_knob_clicked;
extern bool prev_blue_knob_clicked;

extern bool red_knob_clicked;
extern bool green_knob_clicked;
extern bool blue_knob_clicked;

extern volatile uint32_t* knobs_state;

extern uint8_t knob_data; 
extern uint8_t red_knob_data; 
extern uint8_t green_knob_data; 
extern uint8_t blue_knob_data; 

extern uint8_t prev_red_knob_data;
extern uint8_t prev_green_knob_data;
extern uint8_t prev_blue_knob_data;

extern int8_t red_difference;
extern int8_t green_difference;
extern int8_t blue_difference;

extern volatile uint32_t* led_line;
extern volatile uint32_t* rgb1;
extern volatile uint32_t* rgb2;

//extern bool active_led;


// ############################################################
// ### -----------------INITIALIZATION----------------------###
// ############################################################

/**
 * @brief Initializes the memory for peripherals
 * 
 */
void init_peripherals_memory();

/**
 * @brief LED Initialization
 * 
 */
void init_leds();

/**
 * @brief Knobs Initialization
 * 
 */
void init_knobs();

/**
 * @brief Screen Initialization
 * 
 */
void init_screen();

/**
 * @brief Font Initialization
 * 
 */
void init_font();

// ############################################################
// ### -----------------KNOB FUNCTIONS----------------------###
// ############################################################

/**
 * @brief Updates knob values 
 * 
 */
void update_knobs();

/**
 * @brief Computes the distance of the knob rotation
 * 
 * @param current_data Current knob value
 * @param prev_data Previous knob value
 * @return Difference between these two values
 */
uint8_t compute_difference(uint8_t current_data, uint8_t prev_data);

// ############################################################
// ### ------------------LED FUNCTIONS----------------------###
// ############################################################

/**
 * @brief Changes the color of the first led
 * 
 * @param mem_base 
 * @param color 
 */
void change_rgb1(uint8_t *mem_base, uint32_t color);

/**
 * @brief Changes the color of the first led
 * 
 * @param mem_base 
 * @param color 
 */
void change_rgb2(uint8_t *mem_base, uint32_t color);

/**
 * @brief Shutting down the active leds
 * 
 */
void kill_leds();

uint32_t set_hex_color_val(uint8_t red_val, uint8_t green_val, uint8_t blue_val);

// ############################################################
// ### ---------------SCREEN FUNCTIONS----------------------###
// ############################################################

/**
 * @brief Draws a pixel on screen
 * 
 * @param x x coordinate
 * @param y y coordinate
 * @param color color of the pixel in hex 16 bit
 */
void draw_pixel(int x, int y, uint16_t color);

/**
 * @brief Draws a scaled pixel on screen
 * 
 * @param x x coordinate
 * @param y y coordinate
 * @param color color of the pixel in hex 16 bit
 * @param scale scale value
 */
void draw_scaled_pixel(int x, int y, uint16_t color, int scale);

/**
 * @brief Fills every pixel on screen with black color
 * 
 */
void clear_screen();

/**
 * @brief Send the data into LCD screen to display it
 * 
 * @param lcd_mem pointer to lcd memory
 */
void display_on_screen(uint8_t *lcd_mem);

/**
 * @brief Display scaled pixel on cavnas and also save it to memory
 * 
 * @param x
 * @param y
 * @param color Color of the pixel
 * @param scale Scale of the pixel
 */
void draw_canvas_scaled_pixel(int x, int y, uint16_t color, int scale);

/**
 * @brief Save drawn image
 *
 */
void save_image_memory();

#endif /* UTILS_H */ 
